import * as path from 'path';
import { defineConfig } from 'vite';
import react from "@vitejs/plugin-react";
import tsConfigPath from "vite-tsconfig-paths";
import dts from "vite-plugin-dts";
import { VitePWA } from "vite-plugin-pwa";
import loadEnv from './loadEnv';

// Load environment variables from .env.sample
const env = loadEnv();

export default defineConfig({
  plugins: [
    react({
      include: "**/*.tsx",
    }),
    tsConfigPath(),
    VitePWA({
      includeAssets: ["smg-logo.png"],
      manifest: {
        name: "Base",
        short_name: "Base",
        theme_color: "#0050B3",
        icons: [
          {
            src: "/smg-logo.png",
            sizes: "192x192",
            type: "image/png",
          },
          {
            src: "/smg-logo.png",
            sizes: "512x512",
            type: "image/png",
          },
        ],
      },
    }),
    dts({
      insertTypesEntry: true,
    }),
  ],
  css: {
    preprocessorOptions: {
      less: {
        javascriptEnabled: true,
      },
    },
  },
  resolve: {
    alias: [{ find: "src", replacement: path.resolve(__dirname, "src") }],
  },
  define: {
    // Use the environment variables loaded from .env.sample
    'process.env': env,
  },
  server: {
    // Disable HMR (Hot Module Replacement) or Disable Refresh Web on Browser
    hmr: false,
  },
});
