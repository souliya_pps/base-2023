import * as fs from 'fs';
import * as path from 'path';

function loadEnv(): Record<string, string> {
    const envSamplePath = path.resolve(__dirname, '.env.sample');
    const envSampleContents = fs.readFileSync(envSamplePath, 'utf8');
    const lines = envSampleContents.split('\n');
    const envVars: Record<string, string> = {};

    for (const line of lines) {
        const trimmedLine = line.trim();
        if (trimmedLine && !trimmedLine.startsWith('#')) {
            const [key, value] = trimmedLine.split('=');
            envVars[key] = value;
        }
    }

    return envVars;
}

export default loadEnv;
