import "antd/dist/antd.css";
import i18next from "i18next";
import ReactDOM from "react-dom/client";
import App from "src/App";
import "src/ui/config/utils/i18n";
import { lang } from "./service/cache/getItemLocalStorage";
import { QueryClient, QueryClientProvider } from "@tanstack/react-query";
import { BrowserRouter } from "react-router-dom";
// import { ReactQueryDevtools } from "@tanstack/react-query-devtools";
// import "src/ui/styles/theme/fonts.css";

i18next.changeLanguage(lang);

const queryClient = new QueryClient();

ReactDOM.createRoot(document.getElementById("root") as HTMLElement).render(
  <BrowserRouter>
    <QueryClientProvider client={queryClient}>
      <App />
      {/* <ReactQueryDevtools initialIsOpen={false} /> */}
    </QueryClientProvider>
  </BrowserRouter>
);
