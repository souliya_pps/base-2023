import moment, { Moment } from 'moment';

// regex for phone number validation
export const phoneRegex = /^(?:\+856(?:21|23|31|34|36|38|41|51|54|61|64|71|74|81|84|86|88)[2|5|6|7|9|8|4]\d{5}$)|(?:\+856(?:20)[2|5|6|7|9|8|4]\d{7}$)|(?:\+856(?:30)[2|5|6|7|9|8|4]\d{6})$/i;

// regex for pattern passwords
export const passwordRegex = /^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)(?=.*[\W_]).{7,}$/;

// regex for pattern email addresses
export const emailRegex = /^[^\s@]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;

export const formatDayMonthYear = (date: Moment) => {
    return moment(date).format('DD-MM-YYYY');
};

export const formatTime = (date: Moment) => {
    return moment(date).format('HH:mm:ss');
};

export const formatDayMonthYearTime = (date: Moment) => {
    return moment(date).format('DD-MM-YYYY HH:mm:ss');
};

export const validateRequired = (value: string) => !!value.length;

export const validateEmail = (email: string) => {
    !!email.length &&
        email
            .toLowerCase()
            .match(
                /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
            );

};

type FieldValues = Record<string, any>;

export const joinQueryParamsToString = <T extends FieldValues = FieldValues>(
    values: T,
    keysParams?: (keyof T)[]
): string => {
    let query = '';
    if (typeof values !== 'object') return '';
    const keys = keysParams || Object.keys(values);
    keys.forEach((key) => {
        const value = values[key];
        const typeOfValue = typeof value;
        if (
            (typeOfValue === 'boolean' ||
                typeOfValue === 'number' ||
                typeOfValue === 'string') &&
            value &&
            typeof key === 'string'
        ) {
            query = query + `&${key}=${encodeURIComponent(value)}`;
        }
    });
    return query;
};
