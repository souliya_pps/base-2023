import React from 'react';
import {
  FieldValues,
  DefaultValues,
  useForm,
  UseFormReturn,
} from 'react-hook-form';

type ChildrenProp<TFieldValues extends FieldValues = FieldValues> =
  | React.ReactNode
  | ((methods: UseFormReturn<TFieldValues>) => React.ReactNode);

export type FormProps<TFieldValues extends FieldValues = FieldValues> = {
  children: ChildrenProp<TFieldValues>;
  defaultValues?: DefaultValues<TFieldValues>;
  onSubmit: (values: TFieldValues) => void;
} & Omit<
  React.DetailedHTMLProps<
    React.FormHTMLAttributes<HTMLFormElement>,
    HTMLFormElement
  >,
  'children' | 'onSubmit'
>;

const Form = <TFieldValues extends FieldValues = FieldValues>({
  children,
  defaultValues,
  onSubmit,
  ...props
}: FormProps<TFieldValues>) => {
  const methods = useForm<TFieldValues>({ defaultValues });
  return (
    <form {...props} onSubmit={methods.handleSubmit(onSubmit)}>
      {typeof children === 'function' ? children(methods) : children}
    </form>
  );
};
export default Form;
