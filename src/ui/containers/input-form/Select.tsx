import {
  Controller,
  RegisterOptions,
  Control,
  Path,
  FieldError,
  FieldValues,
} from 'react-hook-form';
import InputLabel from '@mui/material/InputLabel';
import Typography from '@mui/material/Typography';
import FormControl from '@mui/material/FormControl';
import MuiSelect, { SelectProps as MuiSelectProps } from '@mui/material/Select';

export type SelectProps<TFormValues extends FieldValues> = {
  name: Path<TFormValues>;
  rules?: RegisterOptions;
  control?: Control<TFormValues>;
  error?: FieldError;
  helperText?: string;
} & Omit<MuiSelectProps, 'name' | 'error'>;

const Select = <TFormValues extends FieldValues>({
  name,
  rules,
  control,
  fullWidth = false,
  label,
  children,
  error,
  ...props
}: SelectProps<TFormValues>) => {
  return (
    <Controller
      name={name}
      control={control}
      rules={rules}
      render={({ field: { value, ...field } }) => (
        <FormControl fullWidth={fullWidth} error={!!error}>
          <InputLabel id={'select-label' + label}>{label}</InputLabel>
          <MuiSelect
            fullWidth={fullWidth}
            label={label}
            {...props}
            {...field}
            value={value || ''}
          >
            {children}
          </MuiSelect>
          {!!error && (
            <Typography
              sx={{
                fontSize: '0.75rem',
                margin: '3px 14px 0px',
                fontWeight: '400',
                lineHeight: '1.66',
                letterSpacing: '0.03333em',
              }}
              color="#ff1744"
            >
              {error?.message}
            </Typography>
          )}
        </FormControl>
      )}
    />
  );
};
export default Select;
