import Typography from '@mui/material/Typography';
import MuiRadio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormControl from '@mui/material/FormControl';
import FormLabel from '@mui/material/FormLabel';
import {
  Controller,
  RegisterOptions,
  Control,
  Path,
  FieldError,
  FieldValues,
} from 'react-hook-form';

interface FormControlLabelProps {
  label: string;
  value: string;
  disabled?: boolean;
  labelPlacement?: 'end' | 'start' | 'top' | 'bottom';
}
export type RadioProps<TFormValues extends FieldValues> = {
  name: Path<TFormValues>;
  rules?: RegisterOptions;
  control?: Control<TFormValues>;
  error?: FieldError;
  options: FormControlLabelProps[];
  required?: boolean;
  row?: boolean;
  label?: string;
};
const Radio = <TFormValues extends FieldValues>({
  name,
  rules,
  control,
  label = '',
  options,
  required = false,
  row = false,
  error,
}: RadioProps<TFormValues>) => {
  return (
    <Controller
      name={name}
      control={control}
      rules={rules}
      render={({ field }) => (
        <FormControl>
          {label && (
            <FormLabel id={label}>
              {label}
              {required && '*'}
            </FormLabel>
          )}
          <RadioGroup row={row} aria-labelledby={label} {...field}>
            {options.map(
              (
                { label, value, labelPlacement = 'end', disabled = false },
                index
              ) => (
                <FormControlLabel
                  key={index.toString() + label}
                  value={value}
                  control={
                    <MuiRadio
                      sx={{
                        color: 'black',
                        ...(!!error && { color: 'red' }),
                      }}
                    />
                  }
                  label={label}
                  labelPlacement={labelPlacement}
                  disabled={disabled}
                />
              )
            )}
          </RadioGroup>
          {!!error && (
            <Typography
              sx={{
                fontSize: '0.75rem',
                margin: '3px 14px 0px',
                fontWeight: '400',
                lineHeight: '1.66',
                letterSpacing: '0.03333em',
              }}
              color="#ff1744"
            >
              {error?.message}
            </Typography>
          )}
        </FormControl>
      )}
    />
  );
};
export default Radio;
