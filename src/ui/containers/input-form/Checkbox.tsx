import {
  Controller,
  RegisterOptions,
  Control,
  Path,
  FieldError,
  FieldValues,
} from 'react-hook-form';
import FormControlLabel from '@mui/material/FormControlLabel';
import MuiCheckbox, {
  CheckboxProps as MuiCheckboxProps,
} from '@mui/material/Checkbox';

export type CheckboxProps<TFormValues extends FieldValues> = {
  name: Path<TFormValues>;
  rules?: RegisterOptions;
  control?: Control<TFormValues>;
  error?: FieldError;
  labelPlacement?: 'end' | 'start' | 'top' | 'bottom';
  label?: string;
} & Omit<MuiCheckboxProps, 'name' | 'error'>;

const Checkbox = <TFormValues extends FieldValues>({
  name,
  rules,
  control,
  label = '',
  labelPlacement = 'end',
  ...props
}: CheckboxProps<TFormValues>) => {
  return (
    <Controller
      name={name}
      control={control}
      rules={rules}
      render={({ field: { value, ...field } }) => (
        <FormControlLabel
          control={
            <MuiCheckbox checked={value || false} {...props} {...field} />
          }
          label={label}
          labelPlacement={labelPlacement}
        />
      )}
    />
  );
};
export default Checkbox;
