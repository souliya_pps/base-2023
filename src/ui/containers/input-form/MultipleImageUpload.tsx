import { FC } from "react";
import { BoxProps } from "@mui/material";
import Input from "@mui/material/Input";
import AddIcon from "@mui/icons-material/Add";
import Typography from "@mui/material/Typography";
import Stack from "@mui/material/Stack";
import { RegisterOptions, UseFormRegister } from "react-hook-form";
import { styled } from "@mui/material/styles";
import { Image, Box } from "src/ui/components";

const StyledBox = styled(Box, {
  shouldForwardProp: (prop) => prop !== "image" && prop !== "error",
})<{
  image?: boolean;
  error?: boolean;
}>(({ image, error }) => ({
  width: "100%",
  height: "100%",
  display: "flex",
  flexDirection: "column",
  justifyContent: "center",
  justifyItems: "center",
  alignItems: "center",
  border: 1,
  borderStyle: "dashed",
  borderColor: "#D9D9D9",
  ...(image && {
    border: 3,
    padding: "5px",
  }),
  ...(error && {
    borderColor: "#FF0000",
  }),
}));

interface Props extends BoxProps {
  name: string;
  rules?: RegisterOptions;
  label?: string;
  required?: boolean;
  helperText?: string;
  files?: FileList;
  register: UseFormRegister<any>;
  loading?: boolean;
  description?: string;
  error?: boolean;
}
/**
 *
 * @param {string} name - name of key value in form data
 */
const MultipleImageUpload: FC<Props> = ({
  name = "",
  rules,
  label,
  files,
  required,
  helperText,
  register,
  loading = false,
  description,
  error,
  ...props
}) => {
  return (
    <Box {...props}>
      {label && (
        <Typography
          sx={{ fontSize: "16px", mb: 1 }}
          variant="body2"
          color="textSecondary"
        >
          {label}
          {required && " *"}
        </Typography>
      )}
      <label>
        <StyledBox
          image={files !== null}
          error={error}
          sx={{
            minWidth: {
              lg: "150px",
              md: "150px",
              sm: "150px",
              xs: "150px",
            },
            minHeight: {
              lg: "100px",
              md: "100px",
              sm: "100px",
              xs: "100px",
            },
            overflow: "auto",
          }}
        >
          <Input
            type="file"
            sx={{ display: "none" }}
            inputProps={{
              accept: "image/jpeg,image/jpg,image/png",
              multiple: true,
            }}
            {...register(`${name}`, rules)}
            disabled={loading}
          />
          {files && Object.keys(files).length !== 0 ? (
            <Stack direction="row" spacing={2} alignItems="flex-start">
              {Object.keys(files).map((image, index) => (
                <Box
                  key={index}
                  sx={{
                    width: "100%",
                    height: "100%",
                    backgroundColor: "#ecf0f3",
                    padding: "6px",
                    shadow: "0px 0px 5px rgba(0, 0, 0, 0.25)",
                    display: "flex",
                    overflow: "auto",
                  }}
                >
                  <Image
                    src={window.URL.createObjectURL(
                      files[image as any] as File
                    )}
                    alt="preview"
                    sx={{
                      width: "100%",
                    }}
                  />
                </Box>
              ))}
            </Stack>
          ) : (
            <>
              <AddIcon
                fontSize="large"
                sx={{
                  ...(error && {
                    color: "#FF0000",
                  }),
                }}
              />
              <Typography
                color="textSecondary"
                sx={{
                  ...(error && {
                    color: "#FF0000",
                  }),
                }}
              >
                {description ? description : "Upload"}
              </Typography>
            </>
          )}
        </StyledBox>
      </label>
      {helperText && (
        <Typography
          sx={{
            fontSize: "0.75rem",
            margin: "3px 14px 0px",
            fontWeight: "400",
            lineHeight: "1.66",
            letterSpacing: "0.03333em",
          }}
          color="#ff1744"
        >
          {helperText}
        </Typography>
      )}
    </Box>
  );
};
export default MultipleImageUpload;
