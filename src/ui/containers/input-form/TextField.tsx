import {
  Controller,
  RegisterOptions,
  Control,
  Path,
  FieldError,
  FieldValues,
} from 'react-hook-form';
import MuiTextField, {
  TextFieldProps as MuiTextFieldProps,
} from '@mui/material/TextField';

export type TextFieldProps<TFormValues extends FieldValues> = {
  name: Path<TFormValues>;
  rules?: RegisterOptions;
  control?: Control<TFormValues>;
  error?: FieldError;
} & Omit<MuiTextFieldProps, 'name' | 'error'>;

const TextField = <TFormValues extends FieldValues>({
  name,
  rules,
  control,
  error,
  ...props
}: TextFieldProps<TFormValues>) => {
  return (
    <Controller
      name={name}
      control={control}
      rules={rules}
      render={({ field: { value, ...field } }) => (
        <MuiTextField
          value={value || ''}
          {...field}
          {...props}
          error={!!error}
          helperText={error?.message}
        />
      )}
    />
  );
};
export default TextField;
