import { forwardRef } from 'react';
import TextField, { TextFieldProps } from '@mui/material/TextField';
import NumberFormat, { InputAttributes } from 'react-number-format';
import {
  Controller,
  RegisterOptions,
  Control,
  Path,
  FieldError,
  FieldValues,
} from 'react-hook-form';

interface CustomProps {
  onChange: (event: { target: { name: string; value: string } }) => void;
  name: string;
}

const NumberFormatCustom = forwardRef<
  NumberFormat<InputAttributes>,
  CustomProps
>(function NumberFormatCustom(props, ref) {
  const { onChange, ...other } = props;
  return (
    <NumberFormat
      {...other}
      getInputRef={ref}
      onValueChange={(values: { value: any; }) => {
        onChange({
          target: {
            name: props.name,
            value: values.value,
          },
        });
      }}
      thousandSeparator
      isNumericString
      decimalScale={4}
    />
  );
});

export type TextFieldNumberFormatProps<TFormValues extends FieldValues> = {
  name: Path<TFormValues>;
  rules?: RegisterOptions;
  control?: Control<TFormValues>;
  error?: FieldError;
} & Omit<TextFieldProps, 'name' | 'error'>;

const TextFieldNumberFormat = <TFormValues extends FieldValues>({
  name,
  rules,
  control,
  variant = 'outlined',
  color = 'primary',
  fullWidth = false,
  required,
  error,
  InputProps,
  ...props
}: TextFieldNumberFormatProps<TFormValues>) => {
  return (
    <Controller
      name={name}
      control={control}
      rules={rules}
      render={({ field: { onChange, onBlur, value, ref } }) => (
        <TextField
          variant={variant}
          color={color}
          fullWidth={fullWidth}
          type="text"
          error={!!error}
          helperText={error?.message}
          name={name}
          onChange={onChange}
          onBlur={onBlur}
          value={value}
          inputRef={ref}
          required={required}
          InputProps={{
            inputComponent: NumberFormatCustom as any,
            ...InputProps,
          }}
          {...props}
        />
      )}
    />
  );
};
export default TextFieldNumberFormat;
