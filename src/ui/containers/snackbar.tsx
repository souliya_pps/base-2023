import React, { useEffect } from "react";
import { useSnackbar } from "notistack";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";
import useAlertStore from "src/service/store/notify";
import { NotifyAlert } from "src/domain/model/notify/type";

interface Props {
  children: React.ReactNode;
}

const SnackbarHook: React.FC<Props> = ({ children }) => {
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const { alerts, removeAlert } = useAlertStore();

  useEffect(() => {
    if (alerts) {
      // Check if alerts is not null
      alerts
        .filter((alert: NotifyAlert) => alert.serviceType === "snackbar")
        .forEach((alert: NotifyAlert) => {
          enqueueSnackbar(alert.message, {
            variant: alert.type || "info", // Use 'info' if type is not provided
            action: (snackbarId) => (
              <IconButton
                onClick={() => {
                  closeSnackbar(snackbarId);
                  removeAlert(); // Clear the alert after displaying and closing
                }}
              >
                <CloseIcon color="inherit" sx={{ color: "white" }} />
              </IconButton>
            ),
          });
        });
    }
  }, [alerts, enqueueSnackbar, closeSnackbar, removeAlert]);

  return <>{children}</>;
};

export default React.memo(SnackbarHook);
