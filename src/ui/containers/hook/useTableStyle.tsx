import MuiThemeTable from "../MuiThemeTable";

export const tableStyle = {
  autoResetPageIndex: true,
  enableTopToolbar: true,
  enablePagination: true,
  enableRowNumbers: true,
  enableEditing: true,
  enableStickyHeader: true,
  enableFullScreenToggle: true,
  enableDensityToggle: true,
  enableHiding: true,
  enableFilters: false,
  enableSorting: false,
  enableColumnFilters: false,
  manualPagination: false,
  enableColumnActions: false,
  createDisplayMode: "modal",
  editDisplayMode: "modal",
  rowNumberDisplayMode: "original",
  getRowId: (row: { id: string }) => row.id,
  mrtTheme: (theme: { palette: { background: { paper: string } } }) => ({
    baseBackgroundColor: theme.palette.background.paper,
  }),
  muiTablePaperProps: {
    sx: {
      px: 2,
      zIndex: 1000,
    },
  },
  muiTableProps: {
    sx: {
      border: "1px solid rgba(81, 81, 81, .5)",
      width: "100%",
      height: "100%",
      overflow: "auto",
      zIndex: 1000,
    },
  },
  muiTableContainerProps: {
    sx: {
      with: "100%",
      height: "70%",
      maxHeight: "100%",
      zIndex: 1000,
    },
  },
  muiTableHeaderProps: {
    sx: {
      position: "sticky",
      top: 0,
      background: "#fff", // Set your desired background color
      zIndex: 1000, // Set a higher zIndex to ensure it's above other elements
    },
  },
  muiTableHeadCellProps: {
    sx: (theme: { palette: { text: { secondary: string } } }) => ({
      color: theme.palette.text.secondary,
      width: "max-content",
      maxHeight: "auto",
      fontWeight: "bold",
      textAlign: "center",
      justifyContent: "center",
      overflow: "auto",
    }),
  },
  muiTableBodyCellProps: {
    sx: {
      width: "max-content",
      maxHeight: "auto",
      textAlign: "center",
      justifyContent: "center",
      border: "1px solid rgba(81, 81, 81, .5)",
      overflow: "auto",
    },
  },
  muiTableButtonProps: {
    sx: {
      overflow: "auto",
      maxHeight: "auto",
    },
  },
  muiTableFooterProps: {
    sx: {
      maxHeight: "auto",
      position: "sticky",
      background: "#fff", // Set your desired background color
    },
  },
  muiTableBottomToolbarProps: {
    sx: {
      position: "sticky",
      bottom: 0,
      background: "#fff", // Set your desired background color
      zIndex: 1000, // Set a higher zIndex to ensure it's above other elements
    },
  },
  MuiThemeTable: MuiThemeTable,
  initialState: {
    columnPinning: {
      right: ["mrt-row-actions", "state"],
    },
    headerPinning: [
      "mrt-row-numbers",
      "mrt-column-hiding",
      "mrt-column-pinning",
    ],
    body: ["mrt-editing-cell", "mrt-editing-row", "mrt-row-actions"],
    footerPinning: ["mrt-pagination-pinning", "mrt-pagination"],
  },
};
