import NavigateBeforeIcon from "@mui/icons-material/NavigateBefore";
import NavigateNextIcon from "@mui/icons-material/NavigateNext";
import Box from "@mui/material/Box";
import IconButton from "@mui/material/IconButton";
import MenuItem from "@mui/material/MenuItem";
import Select from "@mui/material/Select";
import Typography from "@mui/material/Typography";
import React from "react";

export type PaginationType = {
  pageIndex: number;
  pageSize: number;
};

type PaginationUIProps = {
  pagination: PaginationType;
  totalItems: number;
  pageSizeMenu: PaginationType;
  handlePageSizeChange: (newPageSize: number) => void;
  setPagination: React.Dispatch<React.SetStateAction<PaginationType>>;
};

export function usePaginationUI({
  pagination,
  totalItems,
  pageSizeMenu,
  handlePageSizeChange,
  setPagination,
}: PaginationUIProps) {
  const pageSizeOptions = [25, 50, 100, 200, 300];

  const startIndex = pagination.pageIndex * pagination.pageSize + 1;
  const endIndex =
    (pagination.pageIndex + 1) * pagination.pageSize > totalItems
      ? totalItems
      : (pagination.pageIndex + 1) * pagination.pageSize;

  const handlePrevPage = () => {
    if (pagination.pageIndex > 0) {
      setPagination({
        ...pagination,
        pageIndex: pagination.pageIndex - 1,
      });
    }
  };

  const handleNextPage = () => {
    if ((pagination.pageIndex + 1) * pagination.pageSize < totalItems) {
      setPagination({
        ...pagination,
        pageIndex: pagination.pageIndex + 1,
      });
    }
  };

  return (
    <Box
      sx={{
        display: "flex",
        justifyContent: "right",
        alignItems: "center",
        padding: "8px",
        flexWrap: "wrap",
      }}
    >
      {/* Display the total number of items and the current range */}
      <Typography variant="body2">
        Showing {startIndex} - {endIndex} of {totalItems} items
      </Typography>

      <Box sx={{ margin: "1rem" }}>
        <IconButton
          disabled={pagination.pageIndex === 0}
          onClick={handlePrevPage}
        >
          <NavigateBeforeIcon />
        </IconButton>
        <span>
          Page {pagination.pageIndex + 1} of{" "}
          {Math.ceil(totalItems / pagination.pageSize)}
        </span>
        <IconButton
          disabled={
            (pagination.pageIndex + 1) * pagination.pageSize >= totalItems
          }
          onClick={handleNextPage}
        >
          <NavigateNextIcon />
        </IconButton>
      </Box>

      <Select
        value={pageSizeMenu.pageSize}
        onChange={(event) => {
          const newPageSize = event.target.value as number;
          handlePageSizeChange(newPageSize);
        }}
      >
        {pageSizeOptions.map((pageSize) => (
          <MenuItem key={pageSize} value={pageSize}>
            {pageSize}
          </MenuItem>
        ))}
      </Select>
    </Box>
  );
}
