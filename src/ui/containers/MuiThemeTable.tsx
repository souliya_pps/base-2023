import { ThemeProvider, createTheme } from '@mui/material/styles';
import { red } from '@mui/material/colors';
interface Props {
  children: React.ReactNode;
}

const MuiThemeTable = ({ children }: Props) => {
  const theme = createTheme({
    palette: {
      primary: {
        main: '#1976D2',
        light: '#63a4ff',
        dark: '#004ba0',
        contrastText: '#ffffff',
      },
      secondary: {
        main: '#ffffff',
        contrastText: '#0079a7',
      },
      error: {
        main: red.A400,
        light: '#ff5f52',
        dark: '#c41c00',
        contrastText: '#ffffff',
      },
      background: {
        default: '#fff',
      },
      text: {
        primary: '#1976D2',
        secondary: '#000000',
        disabled: '#8c8c8c',
      },
      common: {
        black: '#fffff',
        white: '#000000',
      },
      success: {
        main: '#66bb6a',
        light: '#81c784',
        dark: '#388e3c',
        contrastText: '#ffffff',
      },
      warning: {
        main: '#ff9800',
        light: '#ffb74d',
        dark: '#f57c00',
        contrastText: '#ffffff',
      },
      info: {
        main: '#2196f3',
        light: '#64b5f6',
        dark: '#1976d2',
        contrastText: '#ffffff',
      },
      mode: 'light',
    },
    components: {
      MuiTableCell: {
        styleOverrides: {
          head: {
            fontFamily: `Defago Noto Sans`,
            fontWeight: 'bold',
          },
          root: {
            fontFamily: `Defago Noto Sans`,
            color: 'black',
          },
        },
      },
    },
  });
  return <ThemeProvider theme={theme}>{children}</ThemeProvider>;
};
export default MuiThemeTable;
