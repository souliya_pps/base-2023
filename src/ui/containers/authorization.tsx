import { FC, ReactNode, useCallback } from "react";
import { Navigate, useLocation } from "react-router-dom";
import useAuthStore from "src/service/store/auth";
import { useFirstRenderEffect } from "./hook";

interface Props {
  children: ReactNode;
}

const Authorization: FC<Props> = ({ children }) => {
  const { isAuthenticated, setTokenState } = useAuthStore();
  const location = useLocation();
  const accessToken = localStorage.getItem("accessToken");
  const refreshToken = localStorage.getItem("refreshToken");

  useFirstRenderEffect({
    effect: useCallback(() => {
      if (accessToken && refreshToken && !isAuthenticated) {
        setTokenState(accessToken, refreshToken);
      }
    }, [accessToken, refreshToken, isAuthenticated]),
  });

  if (!accessToken && location.pathname !== "/login") {
    return <Navigate to="/login" state={{ from: location }} />;
  } else if (isAuthenticated === true && location.pathname === "/login") {
    return <Navigate to="/" state={{ from: location }} />;
  }

  // If the user is authenticated and tries to access the login page, redirect them to the home page

  return <>{children}</>;
};

export default Authorization;
