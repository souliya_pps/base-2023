import {
  ThemeProvider,
  createTheme,
  responsiveFontSizes,
} from "@mui/material/styles";
import { FC } from "react";

import { lightMode } from "./themeConfig";
interface Props {
  children: React.ReactNode;
}
const ToggleColorMode: FC<Props> = (props) => {
  const theme = createTheme(lightMode);
  return (
    <>
      <ThemeProvider theme={responsiveFontSizes(theme)}>
        {props.children}
      </ThemeProvider>
    </>
  );
};
export default ToggleColorMode;
