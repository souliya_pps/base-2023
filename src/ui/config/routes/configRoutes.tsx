import { lazy } from "react";

const Login = lazy(() => import("src/ui/pages/Login"));
const CRUD = lazy(() => import("src/ui/pages/CRUD/CRUD"));
const NetworkStatusAlert = lazy(
  () => import("src/ui/pages/NetworkStatusAlert")
);
const Profile = lazy(() => import("src/ui/pages/Profile/Profile"));

export type LayoutKey = "ADMIN" | "AUTH";

export interface Router {
  component: JSX.Element;
  name: string;
  id: string;
  children?: Router[];
  path: string;
}

export type Routes = {
  [key in LayoutKey]: Array<Router>;
};

const routes: Routes = {
  AUTH: [
    {
      component: <Login />,
      name: "login",
      id: "login",
      path: "/login",
    },
  ],
  ADMIN: [
    // {
    //   component: <Dashboard />,
    //   name: "dashboard",
    //   id: "dashboard",
    //   path: "/",
    // },
    // {
    //   component: <Dashboard />,
    //   name: "dashboard",
    //   id: "dashboard",
    //   path: "/dashboard",
    // },
    {
      component: <CRUD />,
      name: "crud",
      id: "crud",
      path: "/",
    },
    {
      component: <CRUD />,
      name: "crud",
      id: "crud",
      path: "/crud",
    },
    {
      component: <Profile />,
      name: "profile",
      id: "profile",
      path: "/profile",
    },
    {
      component: <NetworkStatusAlert />,
      name: "network-problem",
      id: "network-problem",
      path: "/network-problem",
    },
  ],
};

export default routes;
