import { AccountCircle, ApiRounded } from "@mui/icons-material";
import { ReactNode } from "react";

export interface Navigation {
  id: string;
  name: string;
  icon?: ReactNode;
  to?: string;
  children?: Navigation[];
  targetDimension?: string;
}

const navigations: Array<Navigation> = [
  // {
  //   id: "dashboard",
  //   name: "dashboard",
  //   icon: <Dashboard />,
  //   to: "/dashboard",
  // },
  {
    id: "crud",
    name: "crud",
    icon: <ApiRounded />,
    to: "/crud",
  },
  {
    id: "profile",
    name: "profile",
    icon: <AccountCircle />,
    to: "/profile",
  },
];
export default navigations;
