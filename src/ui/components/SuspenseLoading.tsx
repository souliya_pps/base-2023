import { Box, CircularProgress } from "@mui/material";

const SuspenseLoading = () => {
  return (
    <Box
      sx={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "column",
        backgroundColor: "transparent",
        position: "fixed",
        top: 0,
        right: 0,
        bottom: 0,
        left: 0,
        zIndex: 9999,
      }}
    >
      <Box
        sx={{
          width: {
            md: "80px",
          },
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
        }}
      >
        <CircularProgress sx={{ mt: 3 }} />
      </Box>
    </Box>
  );
};

export default SuspenseLoading;
