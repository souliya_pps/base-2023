import { Avatar, Col, Row } from "antd";
import React from "react";
import "src/ui/styles/loading.css";
import logo from "src/ui/assets/LOGO-SMG.png";

const Loading: React.FC = (): JSX.Element => {
  return (
    <div style={{ height: "100vh" }}>
      <Row
        justify="center"
        align="middle"
        style={{ margin: 0, height: "100%" }}
      >
        <Col>
          <div
            className="lds-ripple"
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Avatar
              className="logo-animation"
              src={logo}
              size="large"
              style={{ width: "90px", height: "90px", zIndex: 1 }}
            />
            <div></div>
            <div></div>
          </div>
        </Col>
      </Row>
    </div>
  );
};

export default Loading;
