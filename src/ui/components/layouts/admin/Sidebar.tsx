import CssBaseline from "@material-ui/core/CssBaseline";
import { makeStyles } from "@material-ui/core/styles";
import ExpandLess from "@mui/icons-material/ExpandLess";
import ExpandMore from "@mui/icons-material/ExpandMore";
import {
  AppBar,
  Box,
  Button,
  Collapse,
  Divider,
  Grid,
  List,
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  Drawer as MuiDrawer,
  Typography,
} from "@mui/material";
import { styled } from "@mui/material/styles";
import clsx from "clsx";
import { ElementType, FC, ReactNode, memo, useState } from "react";
import { useTranslation } from "react-i18next";
import { Link, useLocation } from "react-router-dom";
import navigations, { Navigation } from "src/ui/config/navigations";
import themeSetting from "src/ui/styles/theme/themeConfig";
import BoxScrollbar from "../../page-layout/BoxScrollbar";

const drawerWidth = 240;

const DrawerHeader = styled(AppBar)(({ theme }) => ({
  display: "flex",
  alignItems: "center",
  justifyContent: "flex-start",
  paddingTop: "10px",
  paddingBottom: "10px",
  boxShadow: `-5px 0 5px -5px ${theme.palette.primary.main}, 5px 0 5px -5px ${theme.palette.primary.main}`,
}));

const NavList = styled(List, {
  shouldForwardProp: (prop) => prop !== "item",
})<{
  component?: ElementType;
  item?: boolean;
}>(({ item }) => ({
  "& .MuiListItemButton-root": {
    paddingLeft: 10,
    paddingRight: 10,
  },
  "& .MuiListItemIcon-root": {
    minWidth: 0,
    marginRight: 20,
  },
  "& .MuiSvgIcon-root": {
    fontSize: 20,
  },
  ...(item && {
    "& .MuiListItemButton-root": {
      paddingLeft: 0,
      paddingRight: 0,
    },
    "& .MuiListItemText-root ": {
      paddingLeft: 28,
    },
  }),
}));

const ItemMenu = styled(ListItem, {
  shouldForwardProp: (prop) => prop !== "active",
})<{
  component?: ElementType;
  active?: boolean;
}>(({ active }) => ({
  color: "rgba(0, 0, 0, 0.87)",
  ...(active && {
    backgroundColor: "rgba(25, 118, 210, 0.08)",
    "& .MuiListItemText-root ": {
      color: "#1976D2",
    },
    "& .MuiSvgIcon-root": {
      color: "#1976D2",
    },
  }),
}));

const Img = styled("img")({
  width: "155px",
});

const useStyles = makeStyles((theme) => ({
  brightLight: {
    brightness: "500%",
    opacity: "500%",
  },
  root: {
    display: "flex",
    zIndex: 1,
  },
  appBar: {
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 36,
  },
  hide: {
    display: "none",
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: "nowrap",
  },
  drawerOpen: {
    width: drawerWidth,
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerClose: {
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: "hidden",
    width: theme.spacing(7) + 1,
    [theme.breakpoints.up("sm")]: {
      width: theme.spacing(9) + 1,
    },
  },
  toolbar: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
  toggleButton: {
    position: "absolute",
    top: "10px",
    zIndex: theme.zIndex.drawer + 1,
    transition: "all 225ms cubic-bezier(0.4, 0, 0.6, 1) 0ms",
    "&.MuiIconButton-root:hover": {
      boxShadow: "0 0 0 5px #172b46",
    },
    boxShadow: "0 0 0 2px #172b46",
  },
}));

interface Props {
  openSidebar: boolean;
  onChangeSidebar: () => void;
  breakpoint: boolean;
  closeSidebar: () => void;
}
const Sidebar: FC<Props> = ({ openSidebar, onChangeSidebar, breakpoint }) => {
  const classes = useStyles({ openSidebar });

  return (
    <>
      <div className={classes.root}>
        <CssBaseline />
        <MuiDrawer
          className={clsx(classes.drawer, {
            [classes.drawerOpen]: openSidebar,
            [classes.drawerClose]: !openSidebar,
          })}
          classes={{
            paper: clsx({
              [classes.drawerOpen]: openSidebar,
              [classes.drawerClose]: !openSidebar,
            }),
          }}
          color={themeSetting.sidebar.color}
          sx={{
            width: themeSetting.sidebar.drawerWidth,
            minWidth: themeSetting.sidebar.drawerWidth,
            maxWidth: themeSetting.sidebar.drawerWidth,
            flexShrink: 0,
            "& .MuiDrawer-paper": {
              width: themeSetting.sidebar.drawerWidth,
              boxSizing: "border-box",
              boxShadow: "-5px 0 5px -5px #172b46, 5px 0 5px -5px #172b46",
              // backgroundColor: "#172b46",
            },
            "& ::-webkit-scrollbar": {
              width: "0.1em",
              height: "0.1em",
            },
            "& ::-webkit-scrollbar-thumb": {
              boxShadow: `inset 0 0 0 20px rgba(0, 0, 0, 0.24)`,
              outline: "1px solid slategrey",
              borderRadius: 8,
            },
            "& ::-webkit-scrollbar-thumb:active": {
              boxShadow: `inset 0 0 0 20px rgba(0, 0, 0, 0.37)`,
            },
            "& ::-webkit-scrollbar-track": {
              boxShadow: "inset 0 0 6px rgba(0,0,0,0.00)",
            },
          }}
          PaperProps={{ elevation: 3 }}
          variant={breakpoint ? "temporary" : "persistent"}
          anchor="left"
          open={openSidebar}
          ModalProps={{
            keepMounted: true, // Better open performance on mobile.
          }}
          onClose={onChangeSidebar}
        >
          <DrawerHeader
            position="sticky"
            color={themeSetting.sidebar.header.color}
            elevation={themeSetting.sidebar.header.elevation}
          >
            <Img
              src="/vite.svg"
              alt="logo192"
              className={classes.brightLight}
              sx={{
                width: "50px",
                height: "auto",
                margin: "auto",
              }}
            />
          </DrawerHeader>
          <Divider sx={{ mx: 3, bgcolor: "#ccdcf9" }} />
          <br />
          <BoxScrollbar
            sx={{
              display: "flex",
              flexDirection: "column",
              justifyContent: "space-between",
            }}
          >
            <NavList component="nav" disablePadding aria-labelledby="list-menu">
              {navigations.map((nav) => (
                <Content nav={nav} key={nav.id} openSidebar={openSidebar} />
              ))}
            </NavList>
            <Box>
              <Typography
                variant="body2"
                color="textSecondary"
                align="center"
                className={classes.brightLight}
              >
                Powered by:
              </Typography>
              <Grid
                container
                spacing={0}
                direction="column"
                alignItems="center"
                justifyContent="center"
              >
                <Grid item xs={3}>
                  <Img
                    sx={{ margin: "auto", width: "80px" }}
                    src="/vite.svg"
                    alt="logo192"
                    className={classes.brightLight}
                  />
                </Grid>
              </Grid>
            </Box>
          </BoxScrollbar>
        </MuiDrawer>
      </div>
    </>
  );
};

interface Nav {
  nav: Navigation;
  openSidebar: boolean;
  closeSidebar?: () => void;
}

const Content: FC<Nav> = ({ nav, openSidebar, closeSidebar }) => {
  const { t } = useTranslation("translate");
  const { pathname } = useLocation();
  const [open, setOpen] = useState(false);

  const handleClick = () => {
    setOpen(!open);
  };

  const showIconsOnly = !openSidebar;

  const isActive = nav.to === pathname.slice(0, nav.to?.length);

  return (
    <>
      <ItemMenu
        disablePadding
        active={isActive}
        onClick={closeSidebar}
        sx={{ borderRadius: "8px", padding: "0px" }}
      >
        {showIconsOnly && <ListItemIcon>{nav.icon}</ListItemIcon>}
        <ListItemLink
          onClick={handleClick}
          link={nav.to}
          targetDimension={nav.targetDimension}
        >
          <ListItemIcon>{nav.icon}</ListItemIcon>
          {showIconsOnly ? null : <ListItemText primary={t(nav.name)} />}
          {nav.children && (open ? <ExpandLess /> : <ExpandMore />)}
        </ListItemLink>
      </ItemMenu>
      {nav.children && (
        <Collapse in={open} timeout="auto" unmountOnExit>
          <NavList
            component="nav"
            disablePadding
            aria-labelledby="list-sub-menu"
            item
          >
            {nav.children.map((children) => (
              <Content
                nav={children}
                key={children.id}
                openSidebar={openSidebar}
              />
            ))}
          </NavList>
        </Collapse>
      )}
    </>
  );
};

interface PropsListItemLink {
  children: ReactNode;
  link?: string;
  onClick?: () => void;
  targetDimension?: string;
  rel?: string;
}

const ListItemLink: FC<PropsListItemLink> = ({
  children,
  onClick,
  link,
  targetDimension,
}) => {
  const padding = 10; // Adjust the padding value as needed

  if (targetDimension) {
    return (
      <ListItemButton
        target="_blank"
        rel="noreferrer"
        component={Link}
        to={targetDimension}
        sx={{
          py: 1,
          minHeight: 32,
          paddingLeft: padding,
          paddingRight: padding,
        }}
      >
        {children}
      </ListItemButton>
    );
  } else if (link) {
    return (
      <ListItemButton
        component={Link}
        to={link}
        sx={{
          py: 1,
          minHeight: 32,
          paddingLeft: padding,
          paddingRight: padding,
        }}
      >
        {children}
      </ListItemButton>
    );
  }
  return (
    <ListItemButton
      component={Button}
      onClick={onClick}
      sx={{ py: 1, minHeight: 32, paddingLeft: padding, paddingRight: padding }}
    >
      {children}
    </ListItemButton>
  );
};
export default memo(Sidebar);
