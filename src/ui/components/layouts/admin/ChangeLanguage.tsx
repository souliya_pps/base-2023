import LanguageIcon from "@mui/icons-material/Language";
import { IconButton, Menu, MenuItem } from "@mui/material";
import moment from "moment";
import { useCallback, useRef, useState } from "react";
import { useTranslation } from "react-i18next";
import { useClickAway } from "react-use";
import i18n from "src/ui/config/utils/i18n";

const languageOptions = [
  { key: "en_lang", lang: "en", label: "English", flag: "🇬🇧" },
  { key: "la_lang", lang: "la", label: "ລາວ", flag: "🇱🇦" },
];

export default function ChangeLanguage() {
  const { t } = useTranslation("translate");
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const [isMenuOpen, setIsMenuOpen] = useState(false);

  const menuRef = useRef(null);

  // Use useClickAway to handle clicks outside the menu
  useClickAway(menuRef, () => {
    handleCloseMenu();
  });

  const handleOpenMenu = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
    setIsMenuOpen(true);
  };

  const handleCloseMenu = () => {
    setAnchorEl(null);
    setIsMenuOpen(false);
  };

  const onChangeLanguage = useCallback((lang: string) => {
    localStorage.setItem("lang", lang);
    const momentLocale = lang === "la" ? "lo" : "en-gb";
    i18n.changeLanguage(lang);
    moment.locale(momentLocale);
    handleCloseMenu();
  }, []);

  return (
    <>
      <IconButton
        onMouseEnter={handleOpenMenu}
        onClick={handleOpenMenu}
        size="small"
        color="inherit"
      >
        <LanguageIcon /> {/* Use the LanguageIcon here */}
      </IconButton>
      <Menu
        ref={menuRef}
        anchorEl={anchorEl}
        open={isMenuOpen}
        onClose={handleCloseMenu}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "right", // Position to the right
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "right", // Position to the right
        }}
        PaperProps={{
          style: {
            width: "150px", // Adjust the width as needed
            marginTop: "10px",
          },
          elevation: 0,
          sx: {
            overflow: "visible",
            padding: "4px",
            filter: "drop-shadow(0px 2px 8px rgba(0,0,0,0.32))",
            mt: 0.5,
            color: "#0a1930",
            mr: 1,
            "& .MuiAvatar-root": {
              width: 32,
              height: 32,
              ml: -0.5,
              mr: 1,
              color: "#0a1930",
            },
            "&:before": {
              content: '""',
              display: "block",
              position: "absolute",
              top: 0,
              right: 12,
              width: 10,
              height: 10,
              bgcolor: "#ffff",
              color: "#ffff",
              transform: "translateY(-50%) rotate(45deg)",
              zIndex: 0,
            },
          },
        }}
      >
        <div onMouseLeave={handleCloseMenu}>
          {languageOptions.map((option) => (
            <MenuItem
              key={option.key}
              onClick={() => onChangeLanguage(option.lang)}
            >
              <span
                role="img"
                aria-label={option.label}
                style={{ marginRight: "8px" }}
              >
                {option.flag}
              </span>
              {t(option.label)}
            </MenuItem>
          ))}
        </div>
      </Menu>
    </>
  );
}
