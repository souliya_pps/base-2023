import LogoutIcon from "@mui/icons-material/Logout";
import PersonIcon from "@mui/icons-material/Person";
import { ListItemIcon, Stack, Typography } from "@mui/material";
import Avatar from "@mui/material/Avatar";
import Button from "@mui/material/Button";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import { MouseEvent, useEffect, useRef, useState } from "react";
import { useTranslation } from "react-i18next";
import { useNavigate } from "react-router-dom";
import { useClickAway } from "react-use";
import {
  accessToken,
  profileUrl,
  username,
} from "src/service/cache/getItemLocalStorage";
import { authStateStore } from "src/service/store/getStateStore";
import { actionGetProfile } from "src/service/store/profile/action";
import themeSetting from "src/ui/styles/theme/themeConfig";
import useAuthStore from "src/service/store/auth";
import { profileStore } from "src/service/store/profile";
import { useHandleLogout } from "src/service/store/auth/action";

export default function Logout() {
  const { t } = useTranslation("translate");
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const [isMenuOpen, setIsMenuOpen] = useState(false);
  const navigate = useNavigate();
  const menuRef = useRef(null);
  const url = window.location.pathname;
  const [isNavigatingToProfile, setIsNavigatingToProfile] = useState(false);
  const { getProfile } = actionGetProfile();
  const { LogoutAction } = useHandleLogout();

  // Get the user's profile from the store
  const usernameState = useAuthStore((state) => state.username);
  const profileUrlState = useAuthStore((state) => state.profileUrl);
  const user = profileStore((state) => state.user?.user);

  useEffect(() => {
    if (accessToken) {
      if (usernameState !== "") {
        getProfile(usernameState);
      }

      if (user?.profileUrl !== undefined) {
        authStateStore.setProfileUrl(user?.profileUrl || "");
        localStorage.setItem("profileUrl", user?.profileUrl || "");
      }
    }
  }, [accessToken, usernameState, user?.profileUrl]);

  // Use useClickAway to handle clicks outside the menu
  useClickAway(menuRef, () => {
    if (!isNavigatingToProfile) {
      handleCloseMenu();
    }
  });

  const handleOpenMenu = (event: MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
    setIsMenuOpen(true);
  };

  const handleCloseMenu = () => {
    setAnchorEl(null);
    setIsMenuOpen(false);
  };

  const handleLogout = () => {
    setAnchorEl(null); // Close the menu before performing the logout
    // You can also clear the user profile from the store if needed.
    handleCloseMenu();

    // Trigger the logout function from the store
    LogoutAction();
  };

  useEffect(() => {
    if (url === "/profile") {
      // If navigating to /profile, close the menu and set the flag
      handleCloseMenu();
      setIsNavigatingToProfile(true);
    } else if (isNavigatingToProfile) {
      // If no longer navigating to /profile, unset the flag
      setIsNavigatingToProfile(false);
    }
  }, [url, isNavigatingToProfile, setIsNavigatingToProfile]);

  return (
    <>
      <Button
        aria-label="profile"
        onClick={handleOpenMenu}
        onMouseEnter={handleOpenMenu}
        color={themeSetting.toolbar.textColor}
      >
        <Stack direction="row" alignItems="center">
          <Avatar
            alt="user photo"
            color="info"
            sx={{
              backgroundColor: "#fff",
              color: "black",
              fontSize: "20px",
            }}
            style={{ width: "50px", height: "50px" }}
          >
            {profileUrlState ? (
              <img
                src={profileUrlState}
                alt="user photo"
                style={{ width: "50px", height: "50px" }}
              />
            ) : profileUrl ? (
              <img
                src={profileUrl}
                alt="user photo"
                style={{ width: "50px", height: "50px" }}
              />
            ) : usernameState.charAt(0).toUpperCase() ? (
              ""
            ) : (
              username.charAt(0).toUpperCase()
            )}
          </Avatar>
          &nbsp;&nbsp;
          <Typography variant="subtitle1" sx={{ color: "white" }}>
            {usernameState ? usernameState : username}
          </Typography>
        </Stack>
      </Button>
      <Menu
        id="profile-menu"
        ref={menuRef}
        anchorEl={
          anchorEl
            ? anchorEl
            : document.getElementById("profile-menu")?.parentElement
        }
        open={isMenuOpen}
        onClose={handleCloseMenu}
        PaperProps={{
          elevation: 0,
          sx: {
            overflow: "visible",
            padding: "4px",
            filter: "drop-shadow(0px 2px 8px rgba(0,0,0,0.32))",
            mt: 0.5,
            color: "#0a1930",
            mr: 1,
            "& .MuiAvatar-root": {
              width: 32,
              height: 32,
              ml: -0.5,
              mr: 1,
              color: "#0a1930",
            },
            "&:before": {
              content: '""',
              display: "block",
              position: "absolute",
              top: 0,
              right: 14,
              width: 10,
              height: 10,
              bgcolor: "#ffff",
              color: "#ffff",
              transform: "translateY(-50%) rotate(45deg)",
            },
          },
        }}
        transformOrigin={{ horizontal: "right", vertical: "top" }}
        anchorOrigin={{ horizontal: "right", vertical: "bottom" }}
      >
        <div onMouseLeave={handleCloseMenu}>
          <MenuItem
            id="profile"
            onClick={() => {
              handleCloseMenu();
              navigate("/profile");
            }}
          >
            <ListItemIcon>
              <PersonIcon style={{ color: "#007bff" }} />{" "}
              {/* Profile Icon Color */}
            </ListItemIcon>
            {t("profile")}
          </MenuItem>
          <MenuItem id="logout" onClick={handleLogout}>
            <ListItemIcon>
              <LogoutIcon style={{ color: "#ff0000" }} />{" "}
              {/* Logout Icon Color */}
            </ListItemIcon>
            {t("logout")}
          </MenuItem>
        </div>
      </Menu>
    </>
  );
}
