import FullscreenIcon from "@mui/icons-material/Fullscreen";
import ReorderIcon from "@mui/icons-material/Reorder";
import { IconButton, AppBar as MuiAppBar, Toolbar } from "@mui/material";
import { styled } from "@mui/material/styles";
import { FC, useState } from "react";
import themeSetting from "src/ui/styles/theme/themeConfig";
import ChangeLanguage from "./ChangeLanguage";
import Logout from "./Logout";

const AppBar = styled(MuiAppBar, {
  shouldForwardProp: (prop) => prop !== "open",
})<{
  component?: React.ElementType;
  open?: boolean;
}>(({ theme }) => ({
  position: "inherit",
  height: 64,
  [theme.breakpoints.down("sm")]: {
    position: "fixed",
    width: "100%",
    minWidth: "100%",
    maxWidth: "100%",
    marginBottom: 64,
  },
  [theme.breakpoints.up("md")]: {
    position: "sticky",
    top: 0,
    zIndex: 100,
  },
}));

interface Props {
  openSidebar: boolean;
  closeSidebar: () => void;
}

const ToolbarHeader: FC<Props> = ({ openSidebar, closeSidebar }) => {
  const [hideOnScroll, setHideOnScroll] = useState(false);

  const handleChangeScreen = () => {
    if (!document.fullscreenElement) {
      document.body.requestFullscreen();
      return;
    }
    document.exitFullscreen();
  };

  const handleScroll = () => {
    const shouldHide = window.scrollY > 100;
    setHideOnScroll(shouldHide);
  };

  // Attach scroll event listener
  window.addEventListener("scroll", handleScroll);

  return (
    <AppBar
      color={themeSetting.toolbar.color}
      position="fixed"
      open={openSidebar}
      style={{
        transform: hideOnScroll ? "translateY(-64px)" : "translateY(0)",
      }}
    >
      <Toolbar
        sx={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-between",
        }}
      >
        <div>
          <IconButton aria-label="reorder" onClick={closeSidebar}>
            <ReorderIcon color={themeSetting.toolbar.textColor} />
          </IconButton>
        </div>
        <div>
          <IconButton aria-label="translate" onClick={handleChangeScreen}>
            <FullscreenIcon color={themeSetting.toolbar.textColor} />
          </IconButton>
          &nbsp; &nbsp; &nbsp;
          <ChangeLanguage />
          &nbsp; &nbsp; &nbsp;
          <Logout />
        </div>
      </Toolbar>
    </AppBar>
  );
};

export default ToolbarHeader;
