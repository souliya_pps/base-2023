import { FC, NamedExoticComponent } from "react";
import { Route, Routes } from "react-router-dom";
import Admin from "src/ui/components/layouts/admin";
import Auth from "src/ui/components/layouts/auth";
import routers, { LayoutKey, Router } from "src/ui/config/routes/configRoutes";
import PageNotFound from "src/ui/pages/PageNotFound";

export type Layout = {
  [key in LayoutKey]: NamedExoticComponent;
};
const layout: Layout = {
  ADMIN: Admin,
  AUTH: Auth,
};

const Layouts: FC = () => {
  return (
    <Routes>
      {Object.keys(layout).map((key) => {
        const Layout = layout[key as LayoutKey];
        return (
          <Route path="/" element={<Layout />} key={key}>
            {mapRoutePath(routers[key as LayoutKey])}
          </Route>
        );
      })}
      <Route path="*" element={<PageNotFound />} />
    </Routes>
  );
};

const mapRoutePath = (routes: Array<Router>) => {
  return routes.map((route) => {
    if (route.children) {
      return (
        <Route key={route.id} path={route.path} element={route.component}>
          {mapRoutePath(route.children)}
        </Route>
      );
    }
    return <Route key={route.id} path={route.path} element={route.component} />;
  });
};

export default Layouts;
