import { FC, memo } from "react";
import { Typography, Box, CssBaseline, Stack } from "@mui/material";
import { Outlet } from "react-router-dom";
import { styled } from "@mui/material/styles";

const Main = styled("main", {
  shouldForwardProp: (prop) => prop !== "open",
})<{}>(() => ({
  flexGrow: 1,
  height: "100%",
  minHeight: "100%",
  width: "100%",
  minWidth: "100%",
}));

const Img = styled("img")({
  width: "200px",
  height: "80px",
});

const Auth: FC = (): JSX.Element => {
  return (
    <Box
      id="auth-layout"
      sx={{
        display: "flex",
        flexDirection: "column",
        height: "100%",
        width: "100%",
        position: "relative",
      }}
    >
      <CssBaseline />
      <Main>
        <Outlet />
      </Main>
      <Stack
        direction={{
          xs: "column",
          sm: "row",
          md: "column",
        }}
        spacing={2}
        sx={{
          position: "absolute",
          bottom: { md: "2rem", sm: "0.5rem" },
          display: { sm: "flex", xs: "none" },
          right: 0,
          left: 0,
          justifyContent: "center",
          alignItems: "center",
          fontFamily: "Defago Noto Sans",
        }}
      >
        <Stack direction="row" spacing={2} alignItems="center">
          <Typography
            color="white"
            sx={{
              mt: "0.5rem",
              fontSize: "20px",
            }}
          >
            POWERED BY
          </Typography>
          <Img src="LaoITDev-light.png" alt="LaoITDev" />
        </Stack>
        <Typography color="white" sx={{ fontSize: "20px" }}>
          VERSION 1.0.0
        </Typography>
      </Stack>
    </Box>
  );
};

export default memo(Auth);
