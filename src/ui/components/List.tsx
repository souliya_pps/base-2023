import styled from "@emotion/styled";
import { List } from "@mui/material";

export const MyList = styled(List)(
  ({}) => ({
    '& .MuiListItem-root': {
      paddingTop: '15px'
    },
    '& .MuiDivider-root': {
      marginTop: '5px'
    }
  })
)