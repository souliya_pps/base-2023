import { LoadingButton } from "@mui/lab";
import { Box, Button, Container, Link, Stack } from "@mui/material";
import { useState } from "react";
import { useTranslation } from "react-i18next";
import { UpdateProfileFormProps } from "src/domain/model/profile/type";
import { uploadProfileStore } from "src/service/store/upload";
import { Dialog } from "src/ui/components";
import { emailRegex, phoneRegex } from "src/ui/containers/Utils";
import { Form, TextField } from "src/ui/containers/input-form";

export const UpdateProfileForm = ({
  isUpdating,
  onSubmit,
  onCancel,
  defaultValues,
  open,
}: UpdateProfileFormProps) => {
  const { t } = useTranslation("translate");
  const [imageUrl, setImageUrl] = useState<string | null>(null);
  const [imageBlob, setImageBlob] = useState<Blob | null>(null);

  const handleFileUpload = async (event: File | any) => {
    let file = event.target.files[0];
    const reader = new FileReader();

    // Set File Upload to Global State
    uploadProfileStore.getState().setFileUpload(file);

    reader.onloadend = () => {
      const blob = dataURLtoBlob(reader.result as string);
      setImageBlob(blob);
      setImageUrl(URL.createObjectURL(blob));
    };

    reader.readAsDataURL(file);
  };

  // Function to convert data URL to Blob
  const dataURLtoBlob = (dataURL: string): Blob => {
    const byteString = atob(dataURL.split(",")[1]);
    const mimeString = dataURL.split(",")[0].split(":")[1].split(";")[0];
    const ab = new ArrayBuffer(byteString.length);
    const ia = new Uint8Array(ab);

    for (let i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }

    return new Blob([ab], { type: mimeString });
  };

  return (
    <Dialog
      onClose={onCancel}
      title={t("update_personal_info")}
      open={open}
      actionLoading={isUpdating}
      actions={
        <>
          <LoadingButton
            type="submit"
            form="update-profile-form"
            name="submit"
            variant="contained"
            loading={isUpdating}
          >
            {t("submit")}
          </LoadingButton>
        </>
      }
      isFooter
    >
      <Box sx={{ p: 2 }}>
        <Form
          defaultValues={defaultValues}
          id="update-profile-form"
          onSubmit={onSubmit}
        >
          {({ control, formState: { errors } }) => (
            <Stack direction="column" spacing={2.5}>
              <Container maxWidth="md" sx={{ mt: 8 }}>
                <Stack
                  direction="column"
                  alignItems="center"
                  spacing={2}
                  sx={{
                    display: "flex",
                    justifyContent: "center",
                  }}
                >
                  <label htmlFor="upload-image">
                    <Button variant="contained" component="span">
                      {t("upload_image")}
                    </Button>
                    <input
                      id="upload-image"
                      hidden
                      accept="image/*"
                      type="file"
                      onChange={handleFileUpload}
                    />
                  </label>
                  {imageUrl && (
                    <Box textAlign="center">
                      <Link
                        href={
                          imageBlob ? URL.createObjectURL(imageBlob) : undefined
                        }
                        rel="noopener noreferrer"
                        target="_blank"
                      >
                        <img src={imageUrl} height="150px" />
                      </Link>
                    </Box>
                  )}
                </Stack>
              </Container>
              <TextField
                control={control}
                name="user.displayName"
                label={t("display_name")}
                error={errors.user?.displayName}
                rules={{
                  required: {
                    value: true,
                    message: t("please_enter_your_username"),
                  },
                }}
              />
              <TextField
                control={control}
                name="user.phone"
                label={t("tel_number")}
                error={errors.user?.phone}
                rules={{
                  required: {
                    value: true,
                    message: t("please_enter_phone_number"),
                  },
                  pattern: {
                    value: phoneRegex,
                    message: t("invalid_phone_number"),
                  },
                }}
              />
              <TextField
                control={control}
                name="user.email"
                label={t("email")}
                error={errors.user?.email}
                rules={{
                  required: {
                    value: true,
                    message: t("please_enter_email_address"),
                  },
                  pattern: {
                    value: emailRegex,
                    message: t("please_enter_valid_email_address"),
                  },
                }}
              />
              <TextField
                control={control}
                name="user.description"
                label={t("description")}
                error={errors.user?.description}
                rules={{
                  required: {
                    value: true,
                    message: t("description"),
                  },
                }}
              />
            </Stack>
          )}
        </Form>
      </Box>
    </Dialog>
  );
};
