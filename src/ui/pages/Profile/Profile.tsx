import EditIcon from "@mui/icons-material/Edit";
import PasswordIcon from "@mui/icons-material/Password";
import {
  Box,
  Divider,
  IconButton,
  Paper,
  Stack,
  Typography,
} from "@mui/material";
import { useEffect } from "react";
import { useTranslation } from "react-i18next";
import { IUser } from "src/domain/model/profile/type";
import { username } from "src/service/cache/getItemLocalStorage";
import useDialogStore from "src/service/store/dialog";
import {
  dialogStateStore,
  updateProfileStateStore,
} from "src/service/store/getStateStore";
import {
  profileStore,
  updatePasswordMeStore,
  updateProfileStore,
} from "src/service/store/profile";
import {
  actionGetProfile,
  actionUpdatePasswordMe,
  actionUpdateProfile,
} from "src/service/store/profile/action";
import { uploadProfileStore } from "src/service/store/upload";
import { actionUploadProfile } from "src/service/store/upload/action";
import SuspenseLoading from "src/ui/components/SuspenseLoading";
import PageLayout from "src/ui/components/page-layout/PageLayout";
import UpdatePasswordForm from "./UpdatePasswordForm";
import { UpdateProfileForm } from "./UpdateProfileForm";

function Profile() {
  const { t } = useTranslation("translate");
  const { updatePasswordMe } = actionUpdatePasswordMe();
  const { updateProfile } = actionUpdateProfile();
  const { uploadProfile } = actionUploadProfile();
  const { getProfile } = actionGetProfile();

  const { isDialogOpen, dialogId } = useDialogStore((state) => ({
    isDialogOpen: state.isDialogOpen,
    dialogId: state.dialogId,
  }));

  // Get the user's profile and loading state from the store
  const { user, loading } = profileStore((state) => ({
    user: state.user?.user,
    loading: state.loading,
  }));

  const { file, profileLink } = uploadProfileStore((state) => ({
    file: state.file,
    profileLink: state.profileLink,
  }));

  const { updateProfileValue } = updateProfileStore((state) => ({
    updateProfileValue: state.updateProfileValue?.user,
  }));

  let formData: FormData | any = null;
  formData = new FormData();
  formData.append("profile", file);

  const handlePasswordIconClick = () => {
    // Open the dialog when the PasswordIcon is clicked
    dialogStateStore.openDialog("updatePassword");
  };

  const handleEditIconClick = () => {
    dialogStateStore.openDialog("updateProfile");
  };

  const handleDialogClose = () => {
    // Close the dialog when needed
    dialogStateStore.closeDialog();
  };

  const handleUpdateProfile = (value: IUser) => {
    updateProfileStateStore.setUpdateProfileValue(value);
    // Check if a file was selected for profile image upload
    if (file !== null) {
      // If a file is selected, upload the profile image first
      uploadProfile(formData);
    } else {
      // If no file is selected, update the user profile directly
      updateProfile(value);
    }
  };

  const updateProfileData: IUser = {
    user: {
      displayName: updateProfileValue?.displayName || "",
      phone: updateProfileValue?.phone || "",
      email: updateProfileValue?.email || "",
      description: updateProfileValue?.description || "",
      profileUrl: profileLink || null,
      metadata: null,
    },
  };

  useEffect(() => {
    if (username) {
      getProfile(username);
    }
  }, [username]);

  useEffect(() => {
    if (profileLink !== "") {
      updateProfile(updateProfileData);
      uploadProfileStore.getState().clearState();
    }
  }, [profileLink]);

  return (
    <PageLayout auth minFullScreen bgcolor="#D9D9D9">
      <Paper
        sx={{
          height: "100%",
          width: "100%",
          display: "flex",
          backgroundColor: "#E5E5E5",
          justifyContent: "center",
          alignItems: "center",
          p: {
            xs: 0,
            sm: 4,
          },
        }}
      >
        <Box
          sx={{
            bgcolor: "white",
            width: {
              sm: "800px",
              md: "800px",
              xs: "100%",
            },
            height: {
              sm: "530px",
              xs: "100%",
            },
            borderRadius: {
              sm: "20px",
              xs: "2px",
            },
            paddingX: {
              xs: "10px",
              sm: "20px",
              md: "50px",
            },
            paddingY: {
              xs: "20px",
            },
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
          }}
        >
          <Stack spacing={2} direction="row" justifyContent="space-between">
            <Typography variant="h4" sx={{ fontWeight: "bold", mb: 1 }}>
              {t("user_profile")}
            </Typography>
            <Stack spacing={2} direction="row">
              <IconButton onClick={handleEditIconClick}>
                <EditIcon />
              </IconButton>
              <IconButton onClick={handlePasswordIconClick}>
                <PasswordIcon />
              </IconButton>
            </Stack>
          </Stack>
          <Divider />
          {loading ? (
            <SuspenseLoading />
          ) : (
            <Stack
              direction="column"
              divider={<Divider />}
              spacing={2}
              sx={{ mt: 2, pt: 2 }}
            >
              <Stack
                direction="row"
                justifyContent="space-between"
                alignItems="center"
              >
                <Typography>{t("username")}</Typography>
                <Typography>{user?.username}</Typography>
              </Stack>
              <Stack
                direction="row"
                justifyContent="space-between"
                alignItems="center"
              >
                <Typography>{t("contact_name")}</Typography>
                <Typography>{user?.displayName}</Typography>
              </Stack>
              <Stack
                direction="row"
                justifyContent="space-between"
                alignItems="center"
              >
                <Typography>{t("tel_number")}</Typography>
                <Typography>{user?.phone}</Typography>
              </Stack>
              <Stack
                direction="row"
                justifyContent="space-between"
                alignItems="center"
              >
                <Typography>{t("email")}</Typography>
                <Typography>{user?.email}</Typography>
              </Stack>
            </Stack>
          )}
        </Box>
        <UpdateProfileForm
          open={isDialogOpen && dialogId === "updateProfile"}
          isUpdating={profileStore((state) => state.loading)}
          onCancel={handleDialogClose}
          onSubmit={(value) => handleUpdateProfile(value)}
          defaultValues={{
            user: {
              displayName: user?.displayName || "",
              phone: user?.phone || "",
              email: user?.email || "",
              description: user?.description || "",
              profileUrl: user?.profileUrl || null,
              metadata: null,
            },
          }}
        />
        <UpdatePasswordForm
          open={isDialogOpen && dialogId === "updatePassword"}
          isAfterUpdate={updatePasswordMeStore.getState().isAfterUpdate}
          isCreating={profileStore((state) => state.loading)}
          onCancel={handleDialogClose}
          onSubmit={(value) => updatePasswordMe(value)}
        />
      </Paper>
    </PageLayout>
  );
}

export default Profile;
