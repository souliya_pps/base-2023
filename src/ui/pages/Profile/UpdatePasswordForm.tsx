import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import { LoadingButton } from "@mui/lab";
import { IconButton, InputAdornment, Stack } from "@mui/material";
import { useEffect } from "react";
import { useForm } from "react-hook-form";
import { useTranslation } from "react-i18next";
import {
  IUpdatePasswordForm,
  UpdatePasswordFormProps,
} from "src/domain/model/profile/type";
import { Dialog } from "src/ui/components";
import { TextField } from "src/ui/containers/input-form";

function UpdatePasswordForm({
  open,
  onSubmit,
  isCreating,
  onCancel,
  isAfterUpdate,
}: UpdatePasswordFormProps) {
  const { t } = useTranslation("translate");

  const {
    control,
    formState: { errors },
    setValue,
    watch,
    handleSubmit,
    reset,
  } = useForm<IUpdatePasswordForm>();

  useEffect(() => {
    if (isAfterUpdate === true) {
      reset({
        password: {
          current: "",
          new: "",
          confirm: "",
        },
        showConfirmPassword: false,
        showPassword: false,
        showOldPassword: false,
      });
    }
  }, [isAfterUpdate, reset]);

  return (
    <>
      <Dialog
        open={open}
        onClose={onCancel}
        title={t("update_password")}
        actionLoading={isCreating}
        isFooter
        maxWidth="sm"
        actions={
          <>
            <LoadingButton
              name="submit"
              type="submit"
              form="update-password-admin"
              variant="contained"
              loading={isCreating}
            >
              {t("save")}
            </LoadingButton>
          </>
        }
      >
        <form onSubmit={handleSubmit(onSubmit)} id="update-password-admin">
          <Stack sx={{ px: 2, py: 2 }} spacing={3}>
            <TextField
              label={t("old_password")}
              control={control}
              name="password.current"
              autoComplete="false"
              type={watch().showOldPassword ? "text" : "password"}
              error={errors.password?.confirm}
              rules={{
                required: {
                  value: true,
                  message: t("please_enter_old_password"),
                },
              }}
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle showOldPassword visibility"
                      onClick={() =>
                        setValue("showOldPassword", !watch().showOldPassword)
                      }
                    >
                      {watch().showOldPassword ? (
                        <Visibility />
                      ) : (
                        <VisibilityOff />
                      )}
                    </IconButton>
                  </InputAdornment>
                ),
              }}
            />
            <TextField
              label={t("new_password")}
              control={control}
              name="password.new"
              autoComplete="false"
              type={watch().showPassword ? "text" : "password"}
              error={errors.showPassword}
              rules={{
                required: {
                  value: true,
                  message: t("please_enter_new_password"),
                },
                minLength: {
                  value: 6,
                  message: t("password_more_than_6_characters"),
                },
                maxLength: {
                  value: 99,
                  message: t("password_less_than_99_characters"),
                },
              }}
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle new password visibility"
                      onClick={() =>
                        setValue("showPassword", !watch().showPassword)
                      }
                    >
                      {watch().showPassword ? (
                        <Visibility />
                      ) : (
                        <VisibilityOff />
                      )}
                    </IconButton>
                  </InputAdornment>
                ),
              }}
            />
            <TextField
              label={t("confirm_password")}
              control={control}
              name="password.confirm"
              autoComplete="false"
              type={watch().showConfirmPassword ? "text" : "password"}
              error={errors.confirmPassword}
              rules={{
                required: {
                  value: true,
                  message: t("please_enter_confirmation_password"),
                },
                validate: (value) =>
                  value === watch("password.confirm") || t("invalid_password"),
              }}
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onClick={() =>
                        setValue(
                          "showConfirmPassword",
                          !watch().showConfirmPassword
                        )
                      }
                    >
                      {watch().showConfirmPassword ? (
                        <Visibility />
                      ) : (
                        <VisibilityOff />
                      )}
                    </IconButton>
                  </InputAdornment>
                ),
              }}
            />
          </Stack>
        </form>
      </Dialog>
    </>
  );
}

export default UpdatePasswordForm;
