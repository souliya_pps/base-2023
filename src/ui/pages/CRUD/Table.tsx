import { ContentCopy } from "@mui/icons-material";
import EditIcon from "@mui/icons-material/Edit";
import FileDownloadIcon from "@mui/icons-material/FileDownload";
import {
  Box,
  Button,
  DialogActions,
  DialogContent,
  DialogTitle,
  IconButton,
  Tooltip,
} from "@mui/material";
import {
  MRT_EditActionButtons,
  MaterialReactTable,
  useMaterialReactTable,
  type MRT_ColumnDef,
} from "material-react-table";
import { useMemo, useState } from "react";
import { BusinessOperatingLicenseList } from "src/domain/model/crud/type";
import { tableStyle } from "src/ui/containers/hook/useTableStyle";
import { useDataTable } from "src/ui/pages/CRUD/useDataTable";
import { formatDayMonthYearTime } from "../../containers/Utils";
import { usePaginationUI } from "../../containers/hook/usePaginationUI";

function Table() {
  const {
    t,
    businessOperatingLicensesState,
    loading,
    pagination,
    pageSizeMenu,
    setPagination,
    handlePageSizeChange,
  } = useDataTable();

  const paginationUIProps = {
    pagination,
    totalItems: businessOperatingLicensesState.length,
    pageSizeMenu,
    handlePageSizeChange,
    setPagination,
  };

  const PaginationUI = usePaginationUI(paginationUIProps);

  const [validationErrors, setValidationErrors] = useState<
    Record<string, string | undefined>
  >({});

  const columns = useMemo<MRT_ColumnDef<BusinessOperatingLicenseList>[]>(
    () => [
      {
        accessorKey: "id",
        header: t("id"),
        minSize: 50,
        maxSize: 50,
        enableClickToCopy: true,
        accessorFn: (row) => row.id || "N/A",
      },
      {
        accessorKey: "type",
        header: t("type"),
        enableClickToCopy: true,
        accessorFn: (row) => row.type || "N/A",
      },
      {
        accessorKey: "number",
        header: t("number"),
        enableClickToCopy: true,
        accessorFn: (row) => row.number || "N/A",
      },
      {
        accessorKey: "holder",
        header: t("holder"),
        enableClickToCopy: true,
        accessorFn: (row) => row.holder || "N/A",
      },
      {
        accessorKey: "identity",
        header: t("identity"),
        enableClickToCopy: true,
        accessorFn: (row) => row.identity || "N/A",
      },
      {
        accessorKey: "phone",
        header: t("phone"),
        enableClickToCopy: true,
        accessorFn: (row) => row.phone || "N/A",
      },
      {
        accessorKey: "email",
        header: t("email"),
        enableClickToCopy: true,
        muiCopyButtonProps: {
          fullWidth: true,
          startIcon: <ContentCopy />,
          sx: { justifyContent: "flex-start" },
        },
        accessorFn: (row) => row.email || "N/A",
      },
      {
        accessorKey: "createdAt",
        header: t("createdAt"),
        enableClickToCopy: true,
        accessorFn: (row) => formatDayMonthYearTime(row.createdAt) || "N/A",
      },
    ],
    [validationErrors]
  );

  const updatedColumns = columns.map((column) => {
    return {
      ...column,
      Header: () => <div>{t(column.header)}</div>,
    };
  });

  const table = useMaterialReactTable<BusinessOperatingLicenseList>({
    columns: updatedColumns,
    data: businessOperatingLicensesState || [],
    ...(tableStyle as any),
    onEditingRowCancel: () => setValidationErrors({}),
    renderEditRowDialogContent: ({ table, row, internalEditComponents }) => (
      <>
        <DialogTitle variant="h3">{t("edit")}</DialogTitle>
        <DialogContent
          sx={{ display: "flex", flexDirection: "column", gap: "1.5rem" }}
        >
          {internalEditComponents} {/* or render custom edit components here */}
        </DialogContent>
        <DialogActions>
          <MRT_EditActionButtons variant="text" table={table} row={row} />
        </DialogActions>
      </>
    ),
    renderRowActions: ({ row, table }) => (
      <Box sx={{ display: "flex", justifyContent: "center", gap: "1rem" }}>
        <Tooltip title="Edit">
          <IconButton color="info" onClick={() => table.setEditingRow(row)}>
            <EditIcon />
          </IconButton>
        </Tooltip>
      </Box>
    ),
    renderTopToolbarCustomActions: () => (
      <>
        <Box
          sx={{
            display: "flex",
            gap: "16px",
            padding: "8px",
            flexWrap: "wrap",
          }}
        >
          <Button onClick={() => {}} startIcon={<FileDownloadIcon />}>
            {t("export_excel")}
          </Button>
        </Box>
      </>
    ),
    renderBottomToolbar: () => <>{PaginationUI}</>,
    state: {
      isLoading: loading,
      pagination: {
        pageSize: pagination.pageSize,
        pageIndex: pagination.pageIndex,
      },
    },
  });

  return <MaterialReactTable table={table} />;
}

export default Table;
