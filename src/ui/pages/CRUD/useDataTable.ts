import { useCallback, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { BusinessOperatingLicenseList, BusinessOperatingLicenseListResponse } from 'src/domain/model/crud/type';
import { businessOperatingLicenseStore } from 'src/service/store/crud';
import { actionGetBusinessOperatingLicense } from 'src/service/store/crud/action';
import { useFirstRenderEffect } from 'src/ui/containers/hook';
import { PaginationType } from '../../containers/hook/usePaginationUI';

export function useDataTable() {
    // Initialization
    const { t } = useTranslation('translate');
    const { getBusinessOperatingLicense } = actionGetBusinessOperatingLicense();
    const { businessOperatingLicenses, loading } = businessOperatingLicenseStore(
        (state) => ({
            businessOperatingLicenses: state.businessOperatingLicenses?.businessOperatingLicenses,
            loading: state.loading,
        })
    );
    const { nextPageToken } = businessOperatingLicenseStore((state) => ({
        nextPageToken: state.businessOperatingLicenses?.nextPageToken,
    }));

    const [businessOperatingLicensesState, setBusinessOperatingLicenses] = useState<BusinessOperatingLicenseList[]>(businessOperatingLicenses || []);

    const [pagination, setPagination] = useState<PaginationType>({
        pageIndex: 0,
        pageSize: 25,
    });

    const [pageSizeMenu, setPageSizeMenu] = useState<PaginationType>({
        pageIndex: 0,
        pageSize: 25,
    });

    const [isFetchingData, setIsFetchingData] = useState(false);

    // Initial data loading
    useEffect(() => {
        const fetchData = async () => {
            setIsFetchingData(true);

            try {
                const { businessOperatingLicenses: data } = await getBusinessOperatingLicense({
                    businessOperatingLicenses: [],
                    queryParam: { number: '' },
                    pagination: { pageSize: 50, nextPageToken: '', pageIndex: 0 },
                });

                setBusinessOperatingLicenses(data || []);
            } catch (error) {
                console.error('=> Error loading initial data:', error);
            } finally {
                setIsFetchingData(false);
            }
        };

        fetchData();

        // Reset pagination on each useEffect call
        setPagination({ pageIndex: 0, pageSize: 25 });

        // Cleanup function
        return () => {
            // Cancel any ongoing requests when the component unmounts
            setIsFetchingData(false);
        };
    }, []);
    
    // Load more data on pagination change
    const LoadMoreData = useCallback(
        async (newPagination: PaginationType) => {
            if (isFetchingData) return;

            try {
                setIsFetchingData(true);

                const { businessOperatingLicenses: newData } = await getBusinessOperatingLicense({
                    businessOperatingLicenses: businessOperatingLicenses || [],
                    queryParam: { number: '' },
                    pagination: { ...newPagination, nextPageToken: nextPageToken || '' },
                });

                updateBusinessOperatingLicenses(newData || [], newPagination.pageIndex);
            } catch (error) {
                console.error('Error loading more data:', error);
            } finally {
                setIsFetchingData(false);
            }
        },
        [businessOperatingLicenses, nextPageToken, isFetchingData]
    );

    // Update businessOperatingLicensesState after loading more data
    const updateBusinessOperatingLicenses = (
        newData: BusinessOperatingLicenseListResponse['businessOperatingLicenses'],
        pageIndex: number
    ) => {
        setBusinessOperatingLicenses(prevData => (
            pageIndex === 0
                ? newData || []
                : (newData && newData.length > 0) ? [...prevData, ...newData] : prevData
        ));
    };

    // Check conditions and load more data on each render if needed
    const loadMoreDataCallback = useCallback(() => {
        if (isFetchingData) return;

        const lastPage = Math.ceil(
            businessOperatingLicensesState.length / pagination.pageSize
        );

        if (pagination.pageIndex === lastPage - 1 && nextPageToken && nextPageToken !== '') {
            setIsFetchingData(true);

            LoadMoreData({
                pageIndex: pagination.pageIndex + 1,
                pageSize: pagination.pageSize,
            }).finally(() => {
                setIsFetchingData(false);
            });
        }
    }, [pagination.pageIndex, pagination.pageSize, nextPageToken, businessOperatingLicensesState, isFetchingData]);

    // Execute loadMoreDataCallback only on the first render
    useFirstRenderEffect({
        effect: loadMoreDataCallback,
    });

    // Handle page size change
    const handlePageSizeChange = (newPageSize: number) => {
        if (isFetchingData) return;

        setIsFetchingData(true);

        setPageSizeMenu(prevPagination => ({ ...prevPagination, pageSize: newPageSize }));

        getBusinessOperatingLicense({
            businessOperatingLicenses: [],
            queryParam: { number: '' },
            pagination: {
                pageSize: newPageSize,
                nextPageToken: nextPageToken || '',
                pageIndex: pagination.pageIndex || 0,
            },
        }).then(({ businessOperatingLicenses: data }) => {
            setBusinessOperatingLicenses(data || []);
        }).finally(() => {
            setIsFetchingData(false);
        });
    };

    return {
        t,
        businessOperatingLicensesState,
        loading,
        pagination,
        pageSizeMenu,
        isFetchingData,
        setPagination,
        handlePageSizeChange,
    };
}
