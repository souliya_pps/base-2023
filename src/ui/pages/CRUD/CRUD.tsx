import { ApiRounded, CreateRounded, Search } from "@mui/icons-material";
import HomeIcon from "@mui/icons-material/Home";
import { LoadingButton } from "@mui/lab";
import {
  Breadcrumbs,
  Button,
  IconButton,
  Link,
  Paper,
  Stack,
  TextField,
  Typography,
} from "@mui/material";
import { useForm } from "react-hook-form";
import { useTranslation } from "react-i18next";
import { Link as RouterLink, useLocation } from "react-router-dom";
import { Queries } from "src/domain/model/crud/type";
import { PageLayout } from "src/ui/components";
import Table from "./Table";

function CRUD() {
  const { t } = useTranslation("translate");
  const { pathname } = useLocation();

  const { getValues, setValue, watch, handleSubmit } = useForm<Queries>({
    defaultValues: {
      openRightSidebar: false,
    },
  });

  const onChangeRightSidebar = () => {
    const openRightSidebar = watch("openRightSidebar");

    setValue("openRightSidebar", !openRightSidebar);
  };

  const onSubmit = (values: Queries) => {
    setValue("openRightSidebar", false);
    console.log("=> values", values);
  };

  return (
    <PageLayout
      minFullScreen
      openRightSidebar={getValues().openRightSidebar}
      onCloseRightSidebar={onChangeRightSidebar}
      header={
        <Breadcrumbs sx={{ my: 2 }}>
          <Link
            underline="hover"
            sx={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            }}
            color="inherit"
            component={RouterLink}
            to={pathname.replace("/crud", "/")}
          >
            <HomeIcon sx={{ mr: 0.5 }} fontSize="inherit" />
            {t("home")}
          </Link>
          <Typography sx={{ display: "flex", alignItems: "center" }}>
            <ApiRounded sx={{ mr: 0.5 }} fontSize="inherit" />
            {t("crud")}
          </Typography>
        </Breadcrumbs>
      }
      rightSidebar={
        <form
          onSubmit={handleSubmit(onSubmit)}
          autoComplete="off"
          id="form-filter-purchases"
        >
          <Stack direction="column" spacing={3}>
            <TextField label={t("number")} fullWidth />
            <Button
              variant="contained"
              color="primary"
              type="submit"
              form="form-filter-purchases"
            >
              {t("search")}
            </Button>
          </Stack>
        </form>
      }
    >
      <Paper
        sx={{
          display: "flex",
          flexDirection: "column",
          p: 2,
          width: "100%",
          height: "100%",
          overflow: "auto",
        }}
      >
        <Typography py={1} variant="h5" color="gray">
          {t("crud")}
        </Typography>

        <Stack direction="row" spacing={2} justifyContent="right">
          <LoadingButton
            startIcon={<CreateRounded />}
            variant="contained"
            sx={{ my: 1, mx: "auto", mr: 0 }}
            loading={false}
            onClick={() => {}}
          >
            {t("create")}
          </LoadingButton>

          <IconButton
            sx={{ float: "right", marginLeft: "auto" }}
            onClick={onChangeRightSidebar}
          >
            <Search />
          </IconButton>
        </Stack>

        <Table />
      </Paper>
    </PageLayout>
  );
}

export default CRUD;
