import WifiOffIcon from "@mui/icons-material/WifiOff";
import { Button, Result, message } from "antd";
import React, { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { navigateStateStore } from "src/service/store/getStateStore";

const NetworkStatusAlert: React.FC = () => {
  const { t } = useTranslation("localazy");
  const [isOnline, setIsOnline] = useState<boolean>(navigator.onLine);

  const handleOnlineStatusChange = () => {
    setIsOnline(navigator.onLine);
    handleNetworkStatusChange();
  };

  const handleNetworkStatusChange = () => {
    if (isOnline) {
      message.success(t("connected_internet_access"));
      navigateStateStore.navigateToHome();
    }
  };

  useEffect(() => {
    window.addEventListener("online", handleOnlineStatusChange);
    window.addEventListener("offline", handleOnlineStatusChange);

    return () => {
      window.removeEventListener("online", handleOnlineStatusChange);
      window.removeEventListener("offline", handleOnlineStatusChange);
    };
  }, [t, isOnline]);

  const handleCheckConnection = () => {
    setIsOnline(navigator.onLine);
    if (isOnline) {
      message.success(t("connected_internet_access"));
      navigateStateStore.navigateToHome();
    } else {
      message.error(t("currently_offline"));
    }
  };

  return (
    <div>
      {!isOnline && (
        <Result
          icon={<WifiOffIcon style={{ fontSize: "68px", color: "red" }} />}
          title={t("network_problem")}
          subTitle={t("currently_offline")}
          extra={[
            <Button
              type="primary"
              key="check-connection"
              onClick={handleCheckConnection}
            >
              {t("check_connection")}
            </Button>,
          ]}
        />
      )}
    </div>
  );
};

export default NetworkStatusAlert;
