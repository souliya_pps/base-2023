import InfoIcon from "@mui/icons-material/Info";
import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import {
  Box,
  Button,
  IconButton,
  InputAdornment,
  Paper,
  Stack,
  Typography,
} from "@mui/material";
import { styled } from "@mui/material/styles";
import { useForm } from "react-hook-form";
import { useTranslation } from "react-i18next";
import { Auth } from "src/domain/model/auth/type";
import { useHandleLogin } from "src/service/store/auth/action";
import TextField from "src/ui/containers/input-form/TextField";
import ChangeLanguageLogin from "../components/layouts/admin/ChangeLanguageLogin";
import { authStateStore } from "src/service/store/getStateStore";

const Img = styled("img")({
  width: "120px",
  height: "120px",
});

const Login = () => {
  const { t } = useTranslation("translate");
  const {
    control,
    watch,
    setValue,
    handleSubmit,
    formState: { errors },
  } = useForm<Auth>();

  const { handleLogin } = useHandleLogin();

  const onSubmit = (data: Auth) => {
    handleLogin(data);

    localStorage.setItem("username", data?.username);

    authStateStore.setUsername(data?.username);
  };

  return (
    <Paper
      sx={{
        fontFamily: "Defago Noto Sans",
        height: "100vh",
        display: "flex",
        backgroundColor: "#0a1930",
        justifyContent: "center",
        alignItems: "center",
        backgroundPosition: "center",
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
        p: {
          xs: "20px",
          sm: 4,
        },
      }}
    >
      <Box position="absolute" top={0} right={0} padding={2}>
        <ChangeLanguageLogin />
      </Box>
      <form
        onSubmit={handleSubmit(onSubmit)}
        autoComplete="off"
        id="form-login"
      >
        <Stack
          direction="column"
          spacing={{ xs: 3, sm: 4, md: 5 }}
          justifyContent="center"
          justifyItems="center"
          alignContent="center"
          alignItems="center"
          sx={{
            bgcolor: "white",
            paddingX: {
              sm: 6,
              xs: 3,
            },
            paddingY: {
              sm: 30,
              xs: 2,
            },
            width: {
              sm: "450px",
              xs: "100%",
            },
            height: {
              sm: "583px",
              xs: "100%",
            },
            borderRadius: {
              sm: "20px",
              xs: "2px",
            },
          }}
        >
          <Img
            src="smg-logo-login.png"
            alt="login-logo"
            sx={{ width: "55%", height: "auto" }}
          />
          <TextField
            control={control}
            name="username"
            label={t("username")}
            autoComplete="off"
            fullWidth
            error={errors.username}
            rules={{
              required: {
                value: true,
                message: t("please_enter_your_username"),
              },
            }}
            InputLabelProps={{
              style: { color: "#0a1930" }, // Change the label color
            }}
            inputProps={{
              style: { color: "#0a1930" }, // Change the input text color
            }}
            sx={{
              "& .MuiOutlinedInput-root": {
                "& fieldset": {
                  borderColor: "#0a1930",
                },
                "&:hover fieldset": {
                  borderColor: "#0a1930",
                },
                "&.Mui-focused fieldset": {
                  borderColor: "#0a1930",
                },
              },
            }}
          />

          <TextField
            control={control}
            name="password"
            autoComplete="off"
            label={t("password")}
            fullWidth
            error={errors.password}
            rules={{
              required: {
                value: true,
                message: t("please_enter_your_password"),
              },
            }}
            InputLabelProps={{
              style: { color: "#0a1930" }, // Change the label color
            }}
            inputProps={{
              style: { color: "#0a1930" }, // Change the input text color
            }}
            sx={{
              "& .MuiOutlinedInput-root": {
                "& fieldset": {
                  borderColor: "#0a1930",
                },
                "&:hover fieldset": {
                  borderColor: "#0a1930",
                },
                "&.Mui-focused fieldset": {
                  borderColor: "#0a1930",
                },
              },
            }}
            type={watch().showPassword ? "text" : "password"}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <IconButton
                    aria-label="toggle password visibility"
                    onClick={() =>
                      setValue("showPassword", !watch().showPassword)
                    }
                  >
                    {watch().showPassword ? <Visibility /> : <VisibilityOff />}
                  </IconButton>
                </InputAdornment>
              ),
            }}
          />
          <Button fullWidth form="form-login" variant="contained" type="submit">
            {t("submit")}
          </Button>
          <Stack
            direction="row"
            justifyContent="center"
            alignContent="center"
            justifyItems="center"
            alignItems="center"
            spacing={1}
          >
            <InfoIcon />
            <Typography
              sx={{
                color: "#0a1930",
              }}
            >
              {t("contact_admin")}
            </Typography>
          </Stack>
        </Stack>
        <Stack
          sx={{
            width: {
              sm: "20%",
              md: "15%",
              xs: "100%",
            },
          }}
        />
      </form>
    </Paper>
  );
};

export default Login;
