import { Paper } from "@mui/material";

function Dashboard() {
  return (
    <Paper
      sx={{
        fontFamily: "Defago Noto Sans",
        height: "100vh",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        backgroundPosition: "center",
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
        p: {
          xs: "20px",
          sm: 4,
        },
      }}
    >
      Dashboard
    </Paper>
  );
}

export default Dashboard;
