import { Button, Paper } from "@mui/material";
import { Typography, Box } from "@mui/material";
import { useNavigate } from "react-router-dom";
import { useTranslation } from "react-i18next";
import { Result } from "antd";

const PageNotFound = () => {
  const { t } = useTranslation("translate");
  const navigate = useNavigate();

  return (
    <Box
      id="auth-layout"
      sx={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        minHeight: "100vh",
        backgroundColor: "#E5E5E5",
      }}
    >
      <Paper
        sx={{
          width: {
            md: "450px",
            sm: "100vh",
            xs: "100vh",
          },
          height: {
            md: "600px",
            sm: "100vh",
            xs: "100vh",
          },
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          flexDirection: "column",
        }}
      >
        <Result status="404" title="404" style={{ marginTop: "-100px" }} />
        <Typography align="center" variant="h4" sx={{ mb: 4 }}>
          {t("page_not_found")}
        </Typography>
        <Button color="error" variant="outlined" onClick={() => navigate("/")}>
          {t("back")}
        </Button>
      </Paper>
    </Box>
  );
};

export default PageNotFound;
