export type dialogId = null | 'updatePassword' | 'updateProfile';

export type DialogState = {
    isDialogOpen: boolean;
    dialogId: dialogId;
    openDialog: (dialogId: dialogId) => void;
    closeDialog: () => void;
};
