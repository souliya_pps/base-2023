export interface NotifyAlert {
    message: string;
    type?: 'success' | 'error' | 'warning' | 'info';
    duration?: number;
    serviceType: 'snackbar' | 'pageComponent';
}

export interface InitialState {
    alerts: NotifyAlert[] | null;
    addAlert: (payload: NotifyAlert | NotifyAlert[]) => void;
    removeAlert: () => void;
}
