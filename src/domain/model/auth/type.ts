import { AxiosResponse } from "axios";

export interface AuthState {
    isAuthenticated: boolean;
    username: string;
    profileUrl: string;
    setTokenState: (accessToken: string, refreshToken: string) => void;
    setUsername: (username: string) => void;
    setProfileUrl: (profileUrl: string) => void;
    LoginSuccess: () => void;
    LoginError: (error: Error) => void;
    logout: () => void;
}

export type Auth = {
    username: string;
    password: string;
    showPassword: boolean;
};

export interface IAuth {
    username: string;
    password: string;
}

export interface LoginData {
    accessToken: string;
    refreshToken: string;
};


export type ILoginData = {
    data: LoginData
};

export type LoginResponse = AxiosResponse<LoginData>;


