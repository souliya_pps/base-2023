export interface NavigateState {
    navigateToHome: () => void;
    navigateToLogin: () => void;
    navigateToNetworkProblem: () => void;
}