
// Profile State
export interface ProfileState {
    user: IProfileResponse | null;
    loading: boolean;
    Success: (data: IProfileResponse) => void;
    Error: (error: Error) => void;
    setLoading: (loading: boolean) => void;
}

export interface IProfileResponse {
    user: ProfileResponse
}

export interface ProfileResponse {
    id: string
    username: string
    displayName: string
    email: string
    phone: string
    description: string
    status: string
    enabled: boolean
    profileUrl: string | null
    createdAt: string
    updatedAt: string
    metadata: string | null
}

// Update Profile
export interface UpdateProfileState {
    user: IProfileResponse | null;
    loading: boolean;
    updateProfileValue: IUser | null;
    Success: (data: IProfileResponse) => void;
    Error: (error: Error) => void;
    setLoading: (loading: boolean) => void;
    setUpdateProfileValue: (value: IUser) => void
}

export type UpdateProfileFormProps = {
    onSubmit: (values: IUser) => void;
    isUpdating: boolean;
    onCancel: () => void;
    defaultValues?: IUser;
    open: boolean;
};

export interface IUser {
    user: User
}

export interface User {
    displayName: string
    phone: string
    email: string | null
    description: string
    profileUrl: string | null
    metadata: string | null
}

// Update Password State
export interface UpdatePasswordMeState {
    password: IChangePassword | null;
    isAfterUpdate: boolean;
    Success: (data: ChangePasswordResponse) => void;
    Error: (error: Error) => void;
}

export type UpdatePasswordFormProps = {
    onCancel: () => void;
    onSubmit: (data: IUpdatePasswordForm) => void;
    isCreating: boolean;
    isAfterUpdate: boolean;
    open: boolean;
};

export type IUpdatePasswordForm = IChangePassword & {
    confirmPassword: string;
    showPassword: boolean;
    showConfirmPassword: boolean;
    showOldPassword: boolean;
};

export interface IChangePassword {
    password: UpdatePassword;
}

export type UpdatePassword = {
    current: string;
    new: string;
    confirm: string;
};

export interface ChangePasswordResponse {
    code: number
    message: string
    status: string
}




