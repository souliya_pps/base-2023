import { Moment } from "moment";


export type Queries = {
    search: IBusinessOperatingLicenseList;
    openRightSidebar: boolean;
};


export interface BusinessOperatingLicenseState {
    businessOperatingLicenses: BusinessOperatingLicenseListResponse | null;
    loading: boolean;
    pageIndex: number;
    Success: (data: BusinessOperatingLicenseListResponse) => void;
    Error: (error: Error) => void;
    setLoading: (loading: boolean) => void;
    setPageIndex: (pageIndex: number) => void;
}

export interface IBusinessOperatingLicenseList {
    businessOperatingLicenses: BusinessOperatingLicenseList[];
    queryParam: {
        number?: string | null;
    }
    pagination: {
        pageSize: number;
        nextPageToken: string;
        pageIndex: number;
    };
}

export interface BusinessOperatingLicenseList {
    id: string;
    type: string;
    number: string;
    holder: string;
    identity: string;
    phone: string;
    email: string;
    attachments: string[];
    createdAt: Moment;
    updatedAt: Moment;
}

export type BusinessOperatingLicenseListResponse = {
    nextPageToken: string;
    businessOperatingLicenses: BusinessOperatingLicenseList[];
};
