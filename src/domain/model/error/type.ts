export interface ApiResponse<T> {
    data?: T;
    error: Error;
}

export interface IError {
    error: Error
}

export interface Error {
    code: number
    message: string
    status: string
    details: Detail[]
}

export interface Detail {
    "@type": string
    reason: string
    domain: string
}
