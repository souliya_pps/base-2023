
export interface UploadProfileState {
    profileUrl: UploadProfileResponse | null;
    profileLink: string;
    loading: boolean;
    file: File | null;
    clearState: () => void;
    Success: (data: UploadProfileResponse) => void;
    Error: (error: Error) => void;
    setLoading: (loading: boolean) => void;
    setFileUpload: (file: File) => void;
}

export interface IUploadProfile {
    profile: FormData | null;
}

export interface UploadProfileResponse {
    profileUrl: string
}
