import { SnackbarProvider } from "notistack";
import Layout from "src/ui/components/layouts/index";
import Snackbar from "src/ui/containers/snackbar";
import ThemeProvider from "src/ui/styles/theme";
import Authorization from "./ui/containers/authorization";
import { Suspense } from "react";
import Loading from "./ui/components/Loading";

function App() {
  return (
    <ThemeProvider>
      <SnackbarProvider
        preventDuplicate
        maxSnack={8}
        anchorOrigin={{
          vertical: "top",
          horizontal: "right",
        }}
        classes={{
          containerRoot: "alert",
        }}
        autoHideDuration={3000}
      >
        <Snackbar>
          <Authorization>
            <Suspense fallback={<Loading />}>
              <Layout />
            </Suspense>
          </Authorization>
        </Snackbar>
      </SnackbarProvider>
    </ThemeProvider>
  );
}

export default App;
