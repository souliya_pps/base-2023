import { UseMutationResult, useMutation, useQueryClient } from "@tanstack/react-query";
import { IAuth, LoginResponse } from "src/domain/model/auth/type";
import axiosInstance from "../cache/defaultInstance";
import { authStateStore } from "../store/getStateStore";
import { handleNetworkProblem } from './networkUtils';

export function useLoginCallApi(): UseMutationResult<LoginResponse, Error, IAuth> {
    const queryClient = useQueryClient();

    const handleLoginSuccess = (data: LoginResponse) => {
        queryClient.setQueryData(['auth'], data);
        authStateStore.LoginSuccess();
    };

    const loginMutation = useMutation<LoginResponse, Error, IAuth>(
        {
            mutationFn: async (auth: IAuth) => {
                const response = await axiosInstance.post<LoginResponse>('/auth/v1/login', auth);
                return response.data;
            },
            onSuccess: handleLoginSuccess,
            onError: (error: Error) => {
                queryClient.setQueryData(['error'], error);
                authStateStore.LoginError(error);
            },
            onMutate: handleNetworkProblem,
        },
    );

    return loginMutation;
}


export function useLogoutCallApi(): UseMutationResult<void, Error, void, void> {
    const queryClient = useQueryClient();

    const handleLogoutSuccess = () => {
        queryClient.removeQueries();
        authStateStore.logout();
    };

    const logoutMutation = useMutation<void, Error, void, void>(
        {
            mutationFn: async () => {
                await axiosInstance.post<void>('/auth/v1/logout');
            },
            onSuccess: handleLogoutSuccess,
            onError: (error: Error) => {
                queryClient.setQueryData(['error'], error);
            },
            onMutate: handleNetworkProblem,
        },
    );

    return logoutMutation;
}