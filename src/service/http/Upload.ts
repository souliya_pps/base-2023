import { UseMutationResult, useMutation, useQueryClient } from "@tanstack/react-query";
import { IUploadProfile, UploadProfileResponse } from "src/domain/model/upload/type";
import { axiosInstanceFormData } from "../cache/defaultInstance";
import { uploadProfileStore } from "../store/upload";
import { handleNetworkProblem } from "./networkUtils";

export function uploadProfileCallApi(): UseMutationResult<UploadProfileResponse, Error, IUploadProfile> {
    const queryClient = useQueryClient();

    const handleUploadProfileSuccess = (data: UploadProfileResponse) => {
        queryClient.setQueryData(['uploadProfile'], data);
        uploadProfileStore.getState().Success(data);
    };

    const uploadProfileMutation = useMutation<UploadProfileResponse, Error, IUploadProfile>(
        {
            mutationFn: async (formData: IUploadProfile) => {
                const response = await axiosInstanceFormData.post<UploadProfileResponse>(`/resourcemanager/v1/profiles:upload`, formData)
                return response.data;
            },
            onSuccess: handleUploadProfileSuccess,
            onError: (error: Error) => {
                queryClient.setQueryData(['error'], error);
                uploadProfileStore.getState().Error(error)
            },
            onMutate: handleNetworkProblem,
        },
    );

    // Use the status property to set a loading state
    return uploadProfileMutation;
}



