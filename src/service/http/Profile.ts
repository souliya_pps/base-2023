import { UseMutationResult, useMutation, useQueryClient } from "@tanstack/react-query";
import { ChangePasswordResponse, IChangePassword, IProfileResponse, IUser } from "src/domain/model/profile/type";
import { profileStore, updatePasswordMeStore, updateProfileStore } from 'src/service/store/profile';
import axiosInstance from "../cache/defaultInstance";
import { handleNetworkProblem } from './networkUtils';

export function getProfileCallApi(): UseMutationResult<IProfileResponse, Error, string> {
    const queryClient = useQueryClient();

    const handleProfileSuccess = (data: IProfileResponse) => {
        queryClient.setQueryData(['profile'], data);
        profileStore.getState().Success(data);
    };

    const profileMutation = useMutation<IProfileResponse, Error, string>(
        {
            mutationFn: async (username: string) => {
                profileStore.getState().setLoading(true);
                const response = await axiosInstance.get<IProfileResponse>(`/auth/v1/users/${username}`);
                return response.data;
            },
            onSuccess: handleProfileSuccess,
            onError: (error: Error) => {
                queryClient.setQueryData(['error'], error);
                profileStore.getState().Error(error)
            },
            onMutate: handleNetworkProblem,
        },
    );

    // Use the status property to set a loading state
    return profileMutation;
}

export function updateProfileCallApi(): UseMutationResult<IProfileResponse, Error, IUser> {
    const queryClient = useQueryClient();

    const handleUpdateProfileSuccess = (data: IProfileResponse) => {
        queryClient.setQueryData(['updateProfile'], data);
        updateProfileStore.getState().Success(data);
    };

    const updateProfileMutation = useMutation<IProfileResponse, Error, IUser>(
        {
            mutationFn: async (user: IUser) => {
                const response = await axiosInstance.put<IProfileResponse>(`/auth/v1/userInfo`, user)
                return response.data;
            },
            onSuccess: handleUpdateProfileSuccess,
            onError: (error: Error) => {
                queryClient.setQueryData(['error'], error);
                profileStore.getState().Error(error)
            },
            onMutate: handleNetworkProblem,
        },
    );

    // Use the status property to set a loading state
    return updateProfileMutation;
}


export function updatePasswordMeCallApi(): UseMutationResult<ChangePasswordResponse, Error, IChangePassword> {
    const queryClient = useQueryClient();

    const handleUpdatePasswordSuccess = (data: ChangePasswordResponse) => {
        queryClient.setQueryData(['changePasswordMe'], data);
        updatePasswordMeStore.getState().Success(data);
    };

    const updatePasswordMutation = useMutation<ChangePasswordResponse, Error, IChangePassword>(
        {
            mutationFn: async (password: IChangePassword) => {
                const response = await axiosInstance.post<ChangePasswordResponse>(`/auth/v1/changePassword`, password)
                return response.data;
            },
            onSuccess: handleUpdatePasswordSuccess,
            onError: (error: Error) => {
                queryClient.setQueryData(['error'], error);
                profileStore.getState().Error(error)
            },
            onMutate: handleNetworkProblem,
        },
    );

    // Use the status property to set a loading state
    return updatePasswordMutation;
}


