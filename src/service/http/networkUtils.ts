import { navigateStateStore } from '../store/getStateStore';

export function handleNetworkProblem() {
    if (!navigator.onLine) {
        navigateStateStore.navigateToNetworkProblem()
        throw new Error("Network problem");
    }
}

