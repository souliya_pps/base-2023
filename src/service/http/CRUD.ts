import { UseMutationResult, useMutation, useQueryClient } from "@tanstack/react-query";
import { BusinessOperatingLicenseListResponse, IBusinessOperatingLicenseList } from "src/domain/model/crud/type";
import axiosInstance from "../cache/defaultInstance";
import { businessOperatingLicenseStore } from "../store/crud";
import { handleNetworkProblem } from "./networkUtils";

export function getBusinessOperatingLicenseCallApi(): UseMutationResult<BusinessOperatingLicenseListResponse, Error, IBusinessOperatingLicenseList> {
    const queryClient = useQueryClient();

    const handleProfileSuccess = (data: BusinessOperatingLicenseListResponse) => {
        queryClient.setQueryData(['profile'], data);
        businessOperatingLicenseStore.getState().Success(data);
    };

    const businessOperatingLicenseMutation = useMutation<BusinessOperatingLicenseListResponse, Error, IBusinessOperatingLicenseList>(
        {
            mutationFn: async ({
                queryParam,
                pagination
            }: IBusinessOperatingLicenseList) => {
                businessOperatingLicenseStore.getState().setLoading(true);
                const response = await axiosInstance.get<BusinessOperatingLicenseListResponse>(`/resourcemanager/v1/businessOperatingLicenses`, {
                    params: { ...queryParam, ...pagination },
                });
                businessOperatingLicenseStore.getState().setPageIndex(response.config.params.pageIndex);
                businessOperatingLicenseStore.getState().setLoading(false);
                return response.data;
            },
            onSuccess: handleProfileSuccess,
            onError: (error: Error) => {
                queryClient.setQueryData(['error'], error);
                businessOperatingLicenseStore.getState().Error(error)
            },
            onMutate: handleNetworkProblem,
            networkMode: 'online',
        },
    );

    return businessOperatingLicenseMutation;
}


