import { IChangePassword, IProfileResponse, IUser } from 'src/domain/model/profile/type';
import { getProfileCallApi, updatePasswordMeCallApi, updateProfileCallApi } from 'src/service/http/Profile';
import { profileStore, updatePasswordMeStore } from '.';

export function actionGetProfile() {
    const { Success } = profileStore.getState();
    const profileMutation = getProfileCallApi();

    const getProfile = async (username: string) => {
        const profileData: IProfileResponse = await profileMutation.mutateAsync(username);
        Success(profileData);
    };

    return { getProfile };
}

export function actionUpdateProfile() {
    const { Success } = profileStore.getState();
    const updateProfileMutation = updateProfileCallApi();

    const updateProfile = async (user: IUser) => {
        const profileData: IProfileResponse = await updateProfileMutation.mutateAsync(user);
        Success(profileData);
    };

    return { updateProfile };
}

export function actionUpdatePasswordMe() {
    const { Success } = updatePasswordMeStore.getState();
    const updatePasswordMeMutation = updatePasswordMeCallApi();

    const updatePasswordMe = async (password: IChangePassword) => {
        const passwordData = await updatePasswordMeMutation.mutateAsync(password);
        Success(passwordData);
    };

    return { updatePasswordMe };
}

