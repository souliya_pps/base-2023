import { t } from 'i18next';
import { ChangePasswordResponse, IProfileResponse, IUser, ProfileState, UpdatePasswordMeState, UpdateProfileState } from 'src/domain/model/profile/type';
import { create } from 'zustand';
import { alertStateStore, dialogStateStore } from '../getStateStore';
import createSelectors from '../selectors';

// Get Profile

const initialStateProfileStore = {
    user: null,
    loading: false,
};

const useProfileStore = create<ProfileState>((set) => ({
    ...initialStateProfileStore,

    Success: (user: IProfileResponse) => {
        set({ user, loading: false });
    },

    setLoading: (loading: boolean) => {
        set({ loading });
    },

    Error: (error: Error) => {
        alertStateStore.addAlert({
            type: 'error',
            message: t(`${error.message}`),
            serviceType: 'snackbar',
        });
        set({ loading: false });
    },
}));

// Update Profile

const initialStateUpdateProfileStore = {
    user: null,
    updateProfileValue: null,
    loading: false,
};

const useUpdateProfileStore = create<UpdateProfileState>((set) => ({
    ...initialStateUpdateProfileStore,

    Success: (user: IProfileResponse) => {
        set({ user, loading: false });

        alertStateStore.addAlert({
            type: 'success',
            message: t("update_profile_success"),
            serviceType: 'snackbar',
        });

        dialogStateStore.closeDialog();
    },

    setLoading: (loading: boolean) => {
        set({ loading });
    },

    setUpdateProfileValue: (updateProfileValue: IUser) => {
        set({ updateProfileValue })
    },

    Error: (error: Error) => {
        alertStateStore.addAlert({
            type: 'error',
            message: t(`${error.message}`),
            serviceType: 'snackbar',
        });
        set({ loading: false });
    },
}));

//  Update Password Me

const initialStateUpdatePasswordMeStore = {
    password: null,
    isAfterUpdate: false,
};

const useUpdatePasswordMeStore = create<UpdatePasswordMeState>((set) => ({
    ...initialStateUpdatePasswordMeStore,

    Success: (res: ChangePasswordResponse) => {
        set({ isAfterUpdate: true });

        alertStateStore.addAlert({
            type: 'success',
            message: t(`${res.message}`),
            serviceType: 'snackbar',
        });

        dialogStateStore.closeDialog();
    },


    Error: (error: Error) => {
        alertStateStore.addAlert({
            type: 'error',
            message: t(`${error.message}`),
            serviceType: 'snackbar',
        });
    },
}));



export const profileStore = createSelectors(useProfileStore);
export const updateProfileStore = createSelectors(useUpdateProfileStore);
export const updatePasswordMeStore = createSelectors(useUpdatePasswordMeStore);