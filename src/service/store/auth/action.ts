import { IAuth, LoginData, LoginResponse } from 'src/domain/model/auth/type';
import { useLoginCallApi, useLogoutCallApi } from 'src/service/http/Auth';
import { authStateStore } from '../getStateStore';

export function useHandleLogin() {
    const { setTokenState, LoginSuccess } = authStateStore
    const { mutateAsync: login } = useLoginCallApi();

    const handleLogin = async (auth: IAuth) => {
        const data: LoginResponse = await login(auth);
        const { accessToken, refreshToken } = data as unknown as LoginData;

        setTokenState(accessToken, refreshToken);
        LoginSuccess();
    };

    return { handleLogin };
}


export function useHandleLogout() {
    const { logout } = authStateStore
    const { mutateAsync: logoutAPI } = useLogoutCallApi();

    const LogoutAction = async () => {
        await logoutAPI();
        logout();
    };

    return { LogoutAction };
}