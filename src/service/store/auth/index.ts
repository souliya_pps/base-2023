import { t } from 'i18next';
import { AuthState } from 'src/domain/model/auth/type';
import { handleRefreshTokenError, setTokens } from 'src/service/cache/publicInstance';
import { create } from 'zustand';
import { alertStateStore } from '../getStateStore';
import createSelectors from '../selectors';

// Define the initial state
const initialState = {
    isAuthenticated: false,
    username: "",
    profileUrl: "",
};

const useAuthStore = create<AuthState>((set) => ({
    ...initialState,

    setTokenState: (accessToken, refreshToken) => {
        // Set access token and refresh token in Zustand store
        set({ isAuthenticated: true });

        setTokens(accessToken, refreshToken)
    },

    setUsername: (username) => set({ username }),

    setProfileUrl: (profileUrl) => set({ profileUrl }),

    LoginSuccess: async () => {
        // Update the Zustand store and save tokens to localStorage
        set({ isAuthenticated: true });

        // Add an alert to useAlertStore
        alertStateStore.addAlert({
            type: 'success',
            message: t("login_successfully"),
            serviceType: 'snackbar',
        });
    },

    // Method to handle a failed login
    LoginError: (error: Error) => {
        // Update the Zustand store and clear tokens from localStorage
        set({ isAuthenticated: false });

        handleRefreshTokenError();

        // Add an alert to useAlertStore
        alertStateStore.addAlert({
            type: 'error',
            message: t(`${error.message}`),
            serviceType: 'snackbar',
        });
    },


    logout: () => {
        set({ isAuthenticated: false });

        handleRefreshTokenError();

        // Add an alert to useAlertStore
        alertStateStore.addAlert({
            type: 'success',
            message: t("logout_successfully"),
            serviceType: 'snackbar',
        });
    },
}));

export default createSelectors(useAuthStore);
