import { NotifyAlert } from 'src/domain/model/notify/type';
import { create } from 'zustand';
import { InitialState } from 'src/domain/model/notify/type';
import createSelectors from '../selectors';

const initialState = {
  alerts: null,
};

const useAlertStore = create<InitialState>((set) => ({
  ...initialState,

  addAlert: (payload: NotifyAlert | NotifyAlert[]) => {
    set(() => {
      if (Array.isArray(payload)) {
        return { alerts: payload };
      }
      return { alerts: [payload] };
    })
  },

  removeAlert: () => { set(() => ({ alerts: null })) }
}));

export default createSelectors(useAlertStore);

