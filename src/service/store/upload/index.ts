import { t } from "i18next";
import { UploadProfileResponse, UploadProfileState } from "src/domain/model/upload/type";
import { baseURL } from "src/service/cache/defaultInstance";
import { create } from "zustand";
import { alertStateStore } from "../getStateStore";
import createSelectors from "../selectors";

// Upload Profile
const initialStateUploadProfileStore = {
    profileUrl: null,
    profileLink: "",
    file: null,
    loading: false,
};

const useUploadProfileStore = create<UploadProfileState>((set) => ({
    ...initialStateUploadProfileStore,

    Success: (profileUrl: UploadProfileResponse) => {
        const profileLink = `${baseURL}${profileUrl.profileUrl.toString()}`;

        set({ profileLink, loading: false });

        alertStateStore.addAlert({
            type: 'success',
            message: t("upload_profile_success"),
            serviceType: 'snackbar',
        });

        localStorage.setItem('profileUrl', profileLink);
    },

    setFileUpload: (file: File) => {
        set({ file });
    },

    setLoading: (loading: boolean) => {
        set({ loading });
    },

    clearState: () => {
        set({ ...initialStateUploadProfileStore });
    },

    Error: (error: Error) => {
        alertStateStore.addAlert({
            type: 'error',
            message: t(`${error.message}`),
            serviceType: 'snackbar',
        });
        set({ loading: false });
    },
}));


export const uploadProfileStore = createSelectors(useUploadProfileStore);
