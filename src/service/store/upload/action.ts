import { UploadProfileResponse } from "src/domain/model/upload/type";
import { uploadProfileCallApi } from "src/service/http/Upload";
import { uploadProfileStore } from ".";

export function actionUploadProfile() {
    const { Success, setLoading } = uploadProfileStore.getState();
    const uploadProfileMutation = uploadProfileCallApi();

    const uploadProfile = async (file: FormData | any) => {
        setLoading(true);
        const profileData: UploadProfileResponse = await uploadProfileMutation.mutateAsync(file);
        Success(profileData);
    };

    return { uploadProfile };
}