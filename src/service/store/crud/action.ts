import { getBusinessOperatingLicenseCallApi } from "src/service/http/CRUD";
import { businessOperatingLicenseStore } from ".";
import { BusinessOperatingLicenseListResponse, IBusinessOperatingLicenseList } from "src/domain/model/crud/type";

export function actionGetBusinessOperatingLicense() {
    const { Success } = businessOperatingLicenseStore.getState();
    const businessOperatingLicenseDataMutation = getBusinessOperatingLicenseCallApi();

    const getBusinessOperatingLicense = async (data: IBusinessOperatingLicenseList) => {
        const businessOperatingLicenseData: BusinessOperatingLicenseListResponse = await businessOperatingLicenseDataMutation.mutateAsync(data);
        Success(businessOperatingLicenseData);
        return businessOperatingLicenseData;
    };

    return { getBusinessOperatingLicense };
}