import { t } from "i18next";
import { BusinessOperatingLicenseListResponse, BusinessOperatingLicenseState } from "src/domain/model/crud/type";
import { create } from "zustand";
import { alertStateStore } from "../getStateStore";
import createSelectors from "../selectors";

// Get BusinessOperatingLicense

const initialStateBusinessOperatingLicenseStore = {
    businessOperatingLicenses: null,
    loading: false,
    pageIndex: 0,
};

const useBusinessOperatingLicenseStore = create<BusinessOperatingLicenseState>((set) => ({
    ...initialStateBusinessOperatingLicenseStore,

    Success: (businessOperatingLicensesResponse: BusinessOperatingLicenseListResponse) => {
        // Transform the data as needed
        const transformedData = transformData(businessOperatingLicensesResponse);

        // Set the transformed data in the state
        set({ businessOperatingLicenses: transformedData, loading: false });
    },

    setLoading: (loading: boolean) => {
        set({ loading });
    },

    setPageIndex: (index: number) => {
        set({ pageIndex: index });
    },

    Error: (error: Error) => {
        alertStateStore.addAlert({
            type: 'error',
            message: t(`${error.message}`),
            serviceType: 'snackbar',
        });
        set({ loading: false });
    },
}));

const transformData = (data: BusinessOperatingLicenseListResponse) => {
    return data;
};

export const businessOperatingLicenseStore = createSelectors(useBusinessOperatingLicenseStore);
