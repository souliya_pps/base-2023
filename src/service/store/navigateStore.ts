
import { history } from 'src/service/cache/defaultInstance';
import { create } from 'zustand';
import { NavigateState } from '../../domain/model/navigate/type';
import createSelectors from './selectors';

const useNavigateStore = create<NavigateState>(() => ({
    navigateToHome: () => {
        history.push('/');
    },

    navigateToLogin: () => {
        history.push('/login');
    },

    navigateToNetworkProblem: () => {
        history.push('/network-problem');
    },

}));

export default createSelectors(useNavigateStore);