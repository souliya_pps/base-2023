import useAlertStore from 'src/service/store/notify';
import useNavigateStore from 'src/service/store/navigateStore';
import useAuthStore from 'src/service/store/auth';
import useDialogStore from 'src/service/store/dialog';
import { updateProfileStore } from './profile';

export const alertStateStore = useAlertStore.getState()
export const navigateStateStore = useNavigateStore.getState()
export const authStateStore = useAuthStore.getState();
export const dialogStateStore = useDialogStore.getState();
export const updateProfileStateStore = updateProfileStore.getState();
