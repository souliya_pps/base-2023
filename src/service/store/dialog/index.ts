import { DialogState } from 'src/domain/model/dialog/type';
import { create } from 'zustand';
import createSelectors from '../selectors';

const initialState = {
    isDialogOpen: false,
    dialogId: null,
};

const useDialogStore = create<DialogState>((set) => ({
    ...initialState,

    openDialog: (dialogId) => set({ isDialogOpen: true, dialogId }),
    closeDialog: () => set({ isDialogOpen: false, dialogId: null }),
}));

export default createSelectors(useDialogStore);