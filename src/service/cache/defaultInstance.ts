import axios, { Canceler as AxiosCanceler } from 'axios';
import { createBrowserHistory } from 'history';
import { authStateStore } from '../store/getStateStore';
import { refreshToken } from './publicInstance';

export const history = createBrowserHistory();

export const baseURL = process.env.BASE_URL;

// For JSON requests

export const axiosInstance = axios.create({
  baseURL: baseURL,
  headers: { 'Accept': 'application/json', 'Content-Type': 'application/json;charset=utf8' },
});

axiosInstance.interceptors.request.use(
  async (config) => {
    const accessToken = localStorage.getItem('accessToken');
    if (accessToken) {
      config.headers['Authorization'] = `Bearer ${accessToken}`;
    }
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);


axiosInstance.interceptors.response.use(
  (response) => {
    return response;
  },
  async (error) => {
    const originalConfig = error?.config;
    const errorCode = error?.response?.status
    const errorResponse = error?.response?.data?.error

    if (!originalConfig._retry && errorCode && errorCode === 401) {
      originalConfig._retry = true;
      try {
        const accessToken = await refreshToken();
        if (accessToken) {
          originalConfig.headers.Authorization = `Bearer ${accessToken}`;
          return axiosInstance(originalConfig);
        }
      } catch (refreshError) {
        authStateStore.logout();
      }
      authStateStore.logout();
      return Promise.reject(errorResponse);
    }
    return Promise.reject(errorResponse);
  }
);

// For Upload File requests

export const axiosInstanceFormData = axios.create({
  baseURL: baseURL,
  headers: { 'Accept': 'application/json', "Content-Type": "multipart/form-data", },
});

axiosInstanceFormData.interceptors.request.use(
  async (config) => {
    const accessToken = localStorage.getItem('accessToken');
    if (accessToken) {
      config.headers['Authorization'] = `Bearer ${accessToken}`;
    }
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

axiosInstanceFormData.interceptors.response.use(
  (response) => {
    return response;
  },
  async (error) => {
    const originalConfig = error?.config;
    const errorCode = error?.response?.status
    const errorResponse = error?.response?.data?.error

    if (!originalConfig._retry && errorCode && errorCode === 401) {
      originalConfig._retry = true;
      try {
        const accessToken = await refreshToken();
        if (accessToken) {
          originalConfig.headers.Authorization = `Bearer ${accessToken}`;
          return axiosInstanceFormData(originalConfig);
        }
      } catch (refreshError) {
        // authStateStore.logout();
        return Promise.reject(errorResponse);
      }
      authStateStore.logout();
      return Promise.reject(errorResponse);
    }
    return Promise.reject(errorResponse);
  }
);



export const CancelToken = axios.CancelToken;
export const createCancelToken = () => {
  return new CancelToken((cancel: AxiosCanceler) => cancel);
};
export type Canceler = AxiosCanceler;
export default axiosInstance;