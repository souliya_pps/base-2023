import axios from 'axios';
import { LoginResponse } from 'src/domain/model/auth/type';
import { history } from './defaultInstance';

export const publicInstance = axios.create({
    baseURL: process.env.BASE_URL || '',
    headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
});

export const refreshToken = async (): Promise<string | null> => {
    try {
        const { data }: LoginResponse = await publicInstance.post('/auth/v1/refresh', {
            refreshToken: localStorage.getItem('refreshToken') || '',
        });

        setTokens(data.accessToken, data.refreshToken);
        return data.accessToken;
    } catch (error: any) {
        clearTokens();
        return null;
    }
};

export function setTokens(accessToken: string, refreshToken: string) {
    localStorage.setItem('accessToken', accessToken);
    localStorage.setItem('refreshToken', refreshToken);
}

export function clearTokens() {
    localStorage.removeItem('accessToken');
    localStorage.removeItem('refreshToken');
}

export function clearLocalStorage() {
    localStorage.removeItem('TanstackQueryDevtools.open');
    localStorage.removeItem('username');
    localStorage.removeItem('profileUrl');
}

// Function to handle token refresh error
export function handleRefreshTokenError() {
    clearTokens();
    clearLocalStorage();
    return history.push('/login');
}
