export const accessToken = localStorage.getItem('accessToken') as string;
export const refreshToken = localStorage.getItem('refreshToken') as string;
export const username = localStorage.getItem('username') as string;
export const profileUrl = localStorage.getItem('profileUrl') as string;
export const lang = localStorage.getItem("lang") || "la";

